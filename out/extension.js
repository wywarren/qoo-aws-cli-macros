"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const vscode_1 = require("vscode");
const multiStepInput_1 = require("./multiStepInput");
const multiStepInput_2 = require("./multiStepInput");
const multiStepInput_3 = require("./multiStepInput");
const multiStepInput_4 = require("./multiStepInput");
const multiStepInput_5 = require("./multiStepInput");
const multiStepInput_6 = require("./multiStepInput");
const multiStepInput_7 = require("./multiStepInput");
const multiStepInput_8 = require("./multiStepInput");
const multiStepInput_9 = require("./multiStepInput");
const multiStepInput_10 = require("./multiStepInput");
const multiStepInput_11 = require("./multiStepInput");
const multiStepInput_12 = require("./multiStepInput");
const multiStepInput_13 = require("./multiStepInput");
const multiStepInput_14 = require("./multiStepInput");
const multiStepInput_15 = require("./multiStepInput");
const multiStepInput_16 = require("./multiStepInput");
const multiStepInput_17 = require("./multiStepInput");
const multiStepInput_18 = require("./multiStepInput");
const multiStepInput_19 = require("./multiStepInput");
const multiStepInput_20 = require("./multiStepInput");
const multiStepInput_21 = require("./multiStepInput");
const multiStepInput_22 = require("./multiStepInput");
const multiStepInput_23 = require("./multiStepInput");
const multiStepInput_24 = require("./multiStepInput");
const multiStepInput_25 = require("./multiStepInput");
const multiStepInput_26 = require("./multiStepInput");
const multiStepInput_27 = require("./multiStepInput");
const multiStepInput_28 = require("./multiStepInput");
const multiStepInput_29 = require("./multiStepInput");
const multiStepInput_30 = require("./multiStepInput");
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {
    const profileCommandId = 'extension.updateAWSProfile';
    context.subscriptions.push(vscode.commands.registerCommand(profileCommandId, () => {
        const lambdaSteps = multiStepInput_12.multiStepUpdateSettingsProfile(context);
    }));
    multiStepInput_22.profileStatusBarItem.command = profileCommandId;
    context.subscriptions.push(multiStepInput_22.profileStatusBarItem);
    multiStepInput_22.updateStatusBarProfileItem();
    const apiGatewayCommandId = 'extension.updateAPIGateway';
    context.subscriptions.push(vscode.commands.registerCommand(apiGatewayCommandId, () => {
        const lambdaSteps = multiStepInput_20.multiStepUpdateSettingsAPIGateway(context);
    }));
    multiStepInput_23.apiGatewayStatusBarItem.command = apiGatewayCommandId;
    context.subscriptions.push(multiStepInput_23.apiGatewayStatusBarItem);
    multiStepInput_23.updateStatusBarAPIGatewayItem();
    const roleCommandId = 'extension.updateExecutionRole';
    context.subscriptions.push(vscode.commands.registerCommand(roleCommandId, () => {
        const lambdaSteps = multiStepInput_19.multiStepUpdateSettingsExecutionRole(context);
    }));
    multiStepInput_24.roleStatusBarItem.command = roleCommandId;
    context.subscriptions.push(multiStepInput_24.roleStatusBarItem);
    multiStepInput_24.updateStatusBarRoleItem();
    const vpcCommandId = 'extension.updateVPC';
    context.subscriptions.push(vscode.commands.registerCommand(vpcCommandId, () => {
        const lambdaSteps = multiStepInput_11.multiStepUpdateSettingsVPC(context);
    }));
    multiStepInput_21.vpcStatusBarItem.command = vpcCommandId;
    context.subscriptions.push(multiStepInput_21.vpcStatusBarItem);
    multiStepInput_21.updateStatusBarVPCItem();
    const subnetCommandId = 'extension.updateVPCSubnet';
    context.subscriptions.push(vscode.commands.registerCommand(subnetCommandId, () => {
        const lambdaSteps = multiStepInput_13.multiStepUpdateSettingsSubnet(context);
    }));
    multiStepInput_25.subnetStatusBarItem.command = subnetCommandId;
    context.subscriptions.push(multiStepInput_25.subnetStatusBarItem);
    multiStepInput_25.updateStatusBarVPCSubnetItem();
    const securityGroupCommandId = 'extension.updateSecurityGroup';
    context.subscriptions.push(vscode.commands.registerCommand(securityGroupCommandId, () => {
        const lambdaSteps = multiStepInput_14.multiStepUpdateSettingsSecurityGroup(context);
    }));
    multiStepInput_26.securityGroupStatusBarItem.command = securityGroupCommandId;
    context.subscriptions.push(multiStepInput_26.securityGroupStatusBarItem);
    multiStepInput_26.updateStatusBarVPCSecurityGroupItem();
    const layerCommandId = 'extension.updateLambdaLayer';
    context.subscriptions.push(vscode.commands.registerCommand(layerCommandId, () => {
        const lambdaSteps = multiStepInput_15.multiStepUpdateSettingsLambdaLayer(context);
    }));
    multiStepInput_27.layerStatusBarItem.command = layerCommandId;
    context.subscriptions.push(multiStepInput_27.layerStatusBarItem);
    multiStepInput_27.updateStatusBarLayerItem();
    const layersCommandId = 'extension.updateLambdaLayers';
    context.subscriptions.push(vscode.commands.registerCommand(layersCommandId, () => {
        const lambdaSteps = multiStepInput_16.multiStepUpdateSettingsLambdaLayers(context);
    }));
    multiStepInput_28.layersStatusBarItem.command = layersCommandId;
    context.subscriptions.push(multiStepInput_28.layersStatusBarItem);
    multiStepInput_28.updateStatusBarLayersItem();
    const memoryCommandId = 'extension.updateLambdaMemory';
    context.subscriptions.push(vscode.commands.registerCommand(memoryCommandId, () => {
        const lambdaSteps = multiStepInput_17.multiStepUpdateSettingsLambdaMemory(context);
    }));
    multiStepInput_29.memoryStatusBarItem.command = memoryCommandId;
    context.subscriptions.push(multiStepInput_29.memoryStatusBarItem);
    multiStepInput_29.updateStatusBarMemoryItem();
    const timeoutCommandId = 'extension.updateLambdaTimeout';
    context.subscriptions.push(vscode.commands.registerCommand(timeoutCommandId, () => {
        const lambdaSteps = multiStepInput_18.multiStepUpdateSettingsLambdaTimeout(context);
    }));
    multiStepInput_30.timeoutStatusBarItem.command = timeoutCommandId;
    context.subscriptions.push(multiStepInput_30.timeoutStatusBarItem);
    multiStepInput_30.updateStatusBarTimeoutItem();
    /*
    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "qoo-aws-cli-macro" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('extension.createAWSLambdaFunction', () => {
        // The code you place here will be executed every time your command is executed

        let functionName = vscode.window.showInputBox();
        console.log(functionName);
        // Display a message box to the user
        vscode.window.showInformationMessage('Lambda function has been created.');
    });

    context.subscriptions.push(disposable);
    */
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.createAWSLambdaFunction', () => __awaiter(this, void 0, void 0, function* () {
        /*
        const options: { [key: string]: (context: ExtensionContext) => Promise<void> } = {
            showQuickPick,
            showInputBox,
            multiStepInput,
            quickOpen,
        };
        const quickPick = window.createQuickPick();
        quickPick.items = Object.keys(options).map(label => ({ label }));
        quickPick.onDidChangeSelection(selection => {
            if (selection[0]) {
                options[selection[0].label](context)
                    .catch(console.error);
            }
        });
        quickPick.onDidHide(() => quickPick.dispose());
        quickPick.show();
        */
        const lambdaSteps = multiStepInput_1.multiStepCreateLambdaFunction(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateAWSLambdaFunction', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_2.multiStepUpdateLambdaFunction(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.createAWSLambdaFunctionCRUD', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_3.multiStepCreateLambdaFunctionCRUD(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateAWSLambdaFunctionCRUD', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_4.multiStepUpdateLambdaFunctionCRUD(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.addAWSLambdaFunctionAPIGatewayPermission', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_5.multiStepAddLambdaFunctionPermissions(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.removeAWSLambdaFunctionAPIGatewayPermission', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_6.multiStepRemoveLambdaFunctionPermissions(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateLambdaLayerVersion', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_7.multiStepUpdateLambdaLayerVersion(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateLambdaFunctionLayer', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_8.multiStepUpdateLambdaFunctionLayer(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.deployProductionLambdaFunction', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_9.multiStepDeployProductionLambdaFunction(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.openLambdaFunctionAWSConsole', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_10.multiStepOpenLambdaFunctionAWSConsole(context);
    })));
    //settings
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsVPC', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_11.multiStepUpdateSettingsVPC(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsProfile', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_11.multiStepUpdateSettingsVPC(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsSubnet', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_13.multiStepUpdateSettingsSubnet(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsSecurityGroup', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_14.multiStepUpdateSettingsSecurityGroup(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsLambdaLayer', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_15.multiStepUpdateSettingsLambdaLayer(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsLambdaLayers', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_16.multiStepUpdateSettingsLambdaLayers(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsLambdaMemory', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_17.multiStepUpdateSettingsLambdaMemory(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsLambdaTimeout', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_18.multiStepUpdateSettingsLambdaTimeout(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsExecutionRole', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_19.multiStepUpdateSettingsExecutionRole(context);
    })));
    context.subscriptions.push(vscode_1.commands.registerCommand('extension.updateSettingsAPIGateway', () => __awaiter(this, void 0, void 0, function* () {
        const lambdaSteps = multiStepInput_20.multiStepUpdateSettingsAPIGateway(context);
    })));
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map