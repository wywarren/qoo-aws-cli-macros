"use strict";
/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const vscode_1 = require("vscode");
exports.vpcStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.profileStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.apiGatewayStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.roleStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.subnetStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.securityGroupStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.layerStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.layersStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.memoryStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
exports.timeoutStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
function updateStatusBarVPCItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const vpc = vscode.workspace.getConfiguration().get('conf.view.awsVPC');
        exports.vpcStatusBarItem.text = "VPC: " + vpc;
        exports.vpcStatusBarItem.show();
    });
}
exports.updateStatusBarVPCItem = updateStatusBarVPCItem;
function updateStatusBarProfileItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        exports.profileStatusBarItem.text = "Profile: " + profile;
        exports.profileStatusBarItem.show();
    });
}
exports.updateStatusBarProfileItem = updateStatusBarProfileItem;
function updateStatusBarVPCSubnetItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet');
        exports.subnetStatusBarItem.text = "Subnet: " + subnet;
        exports.subnetStatusBarItem.show();
    });
}
exports.updateStatusBarVPCSubnetItem = updateStatusBarVPCSubnetItem;
function updateStatusBarVPCSecurityGroupItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup');
        exports.securityGroupStatusBarItem.text = "Security Group: " + securityGroup;
        exports.securityGroupStatusBarItem.show();
    });
}
exports.updateStatusBarVPCSecurityGroupItem = updateStatusBarVPCSecurityGroupItem;
function updateStatusBarLayerItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const layer = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayer');
        exports.layerStatusBarItem.text = "Layer: " + layer;
        exports.layerStatusBarItem.show();
    });
}
exports.updateStatusBarLayerItem = updateStatusBarLayerItem;
function updateStatusBarLayersItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const layer = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers');
        exports.layersStatusBarItem.text = "Layers: " + layer;
        exports.layersStatusBarItem.show();
    });
}
exports.updateStatusBarLayersItem = updateStatusBarLayersItem;
function updateStatusBarAPIGatewayItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const apiGatewayID = vscode.workspace.getConfiguration().get('conf.view.awsAPIGatewayID');
        exports.apiGatewayStatusBarItem.text = "API: " + apiGatewayID;
        exports.apiGatewayStatusBarItem.show();
    });
}
exports.updateStatusBarAPIGatewayItem = updateStatusBarAPIGatewayItem;
function updateStatusBarRoleItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const role = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole');
        exports.roleStatusBarItem.text = "Role: " + role;
        exports.roleStatusBarItem.show();
    });
}
exports.updateStatusBarRoleItem = updateStatusBarRoleItem;
function updateStatusBarMemoryItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory');
        exports.memoryStatusBarItem.text = "Memory: " + memory + "MB";
        exports.memoryStatusBarItem.show();
    });
}
exports.updateStatusBarMemoryItem = updateStatusBarMemoryItem;
function updateStatusBarTimeoutItem() {
    return __awaiter(this, void 0, void 0, function* () {
        const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout');
        exports.timeoutStatusBarItem.text = "Timeout: " + timeout + "s";
        exports.timeoutStatusBarItem.show();
    });
}
exports.updateStatusBarTimeoutItem = updateStatusBarTimeoutItem;
/**
 * A multi-step input using window.createQuickPick() and window.createInputBox().
 *
 * This first part uses the helper class `MultiStepInput` that wraps the API for the multi-step case.
 */
function multiStepAddLambdaFunctionPermissions(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Add AWS Lambda Function Permissions for API Gateway';
        const numSteps = 6;
        const apiGatewayID = vscode.workspace.getConfiguration().get('conf.view.awsAPIGatewayID');
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const statementID = vscode.workspace.getConfiguration().get('conf.view.awsStatementID');
        class MyButton {
            constructor(iconPath, tooltip) {
                this.iconPath = iconPath;
                this.tooltip = tooltip;
            }
        }
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (profile.length > 0) {
                    settingsReducedSteps++;
                }
                if (region.length > 0) {
                    settingsReducedSteps++;
                }
                if (apiGatewayID.length > 0) {
                    settingsReducedSteps++;
                }
                if (statementID.length > 0) {
                    settingsReducedSteps++;
                }
                yield MultiStepInput.run(input => inputLambdaFunctionName(input, state));
                return state;
            });
        }
        function inputLambdaFunctionName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.functionName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.functionName === 'string' ? state.functionName : '',
                    prompt: 'Enter the name of an existing Lambda function',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputResourcePath(input, state);
            });
        }
        /*
        async function inputAlias(input: MultiStepInput, state: Partial<State>) {
            
            state.functionAlias = await input.showInputBox({
                title,
                step: 2,
                totalSteps: numSteps-settingsReducedSteps,
                value: state.functionAlias || '',
                prompt: 'Enter the name of function alias to grant permission to',
                validate: validateNameIsUnique,
                shouldResume: shouldResume
            });
            
            return (input: MultiStepInput) => inputResourcePath(input, state);
        }
        */
        function inputResourcePath(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.resourcePath = yield input.showInputBox({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.resourcePath || '',
                    prompt: 'Enter the name of the API Gateway resource path to grant permission to',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (apiGatewayID.length > 0) {
                    if (statementID.length > 0) {
                        if (region.length > 0) {
                            if (profile.length > 0) {
                            }
                            else {
                                return (input) => inputProfile(input, state);
                            }
                        }
                        else {
                            return (input) => pickRegion(input, state);
                        }
                    }
                    else {
                        return (input) => inputStatementID(input, state);
                    }
                }
                else {
                    return (input) => inputApiGatewayID(input, state);
                }
            });
        }
        function inputApiGatewayID(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 3;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 3;
                }
                state.apiGatewayID = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.apiGatewayID || '',
                    prompt: 'Enter API Gateway ID',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (statementID.length > 0) {
                    if (region.length > 0) {
                        if (profile.length > 0) {
                        }
                        else {
                            return (input) => inputProfile(input, state);
                        }
                    }
                    else {
                        return (input) => pickRegion(input, state);
                    }
                }
                else {
                    return (input) => inputStatementID(input, state);
                }
            });
        }
        function inputStatementID(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 4;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 3;
                }
                state.statementID = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.statementID || '',
                    prompt: 'Enter a Statement ID',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                    if (profile.length > 0) {
                    }
                    else {
                        return (input) => inputProfile(input, state);
                    }
                }
                else {
                    return (input) => pickRegion(input, state);
                }
            });
        }
        /*
        async function inputAccountID(input: MultiStepInput, state: Partial<State>) {
            
            state.accountID = await input.showInputBox({
                title,
                step: 6,
                totalSteps: numSteps-settingsReducedSteps,
                value: state.accountID || '',
                prompt: 'Enter an Account ID',
                validate: validateNameIsUnique,
                shouldResume: shouldResume
            });
            
            if(region.length>0){
                if(profile.length>0){
                
                }
                else{
                    return (input: MultiStepInput) => inputProfile(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => pickRegion(input, state);
            }
        }
        */
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined /* TODO: token */);
                state.region = yield input.showQuickPick({
                    title,
                    step: 5,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
                if (profile.length > 0) {
                }
                else {
                    return (input) => inputProfile(input, state);
                }
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 6,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.profile || '',
                    prompt: 'Select an AWS profile (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRuntimes(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['python3.6']
                    .map(label => ({ label }));
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        const util = require('util');
        const exec = util.promisify(require('child_process').exec);
        let cliFunctionName = '';
        cliFunctionName = state.functionName;
        //cliFunctionAlias = state.functionAlias;
        let cliAPIGatewayResourcePath = '';
        cliAPIGatewayResourcePath = state.resourcePath;
        let cliAccountID = '';
        let cliStatementID = '';
        if (statementID.length > 0)
            cliStatementID = statementID;
        else
            cliStatementID = state.statementID;
        let cliAPIGatewayID = '';
        if (apiGatewayID.length > 0)
            cliAPIGatewayID = apiGatewayID;
        else
            cliAPIGatewayID = state.apiGatewayID;
        let cliProfile = '';
        if (profile.length > 0)
            cliProfile = profile;
        else
            cliProfile = state.profile;
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec('aws sts get-caller-identity --profile ' + cliProfile);
                //console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                const response = JSON.parse(stdout);
                const accountID = response.Account;
                cliAccountID = String(accountID);
                let cliStatementIDD = cliStatementID + '-1';
                let cliFunctionAlias = 'Production';
                let cliCommand = 'aws lambda add-permission  --function-name "arn:aws:lambda:' +
                    cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName + ':' + cliFunctionAlias + '"' +
                    ' --source-arn "arn:aws:execute-api:' + cliRegion + ':' + cliAccountID + ':' + cliAPIGatewayID + '/*/' + cliAPIGatewayResourcePath +
                    '" --principal apigateway.amazonaws.com --statement-id ' + cliStatementIDD +
                    ' --action lambda:InvokeFunction';
                cliCommand += ' --region ' + cliRegion;
                cliCommand += ' --profile ' + cliProfile;
                vscode_1.window.showInformationMessage(`Adding Lambda Function '${cliFunctionName}:${cliFunctionAlias}' permissions with ${cliCommand}`);
                addLambdaFunctionPermission(cliCommand, cliFunctionName, cliFunctionAlias);
                cliStatementIDD = cliStatementID + '-2';
                cliFunctionAlias = 'Development';
                cliCommand = 'aws lambda add-permission  --function-name "arn:aws:lambda:' +
                    cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName + ':' + cliFunctionAlias + '"' +
                    ' --source-arn "arn:aws:execute-api:' + cliRegion + ':' + cliAccountID + ':' + cliAPIGatewayID + '/*/' + cliAPIGatewayResourcePath +
                    '" --principal apigateway.amazonaws.com --statement-id ' + cliStatementIDD +
                    ' --action lambda:InvokeFunction';
                cliCommand += ' --region ' + cliRegion;
                cliCommand += ' --profile ' + cliProfile;
                vscode_1.window.showInformationMessage(`Adding Lambda Function '${cliFunctionName}:${cliFunctionAlias}' permissions with ${cliCommand}`);
                addLambdaFunctionPermission(cliCommand, cliFunctionName, cliFunctionAlias);
                return accountID;
            });
        }
        function addLambdaFunctionPermission(cmd, functionName, alias) {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec(cmd);
                console.log('stdout:', stdout);
                console.log('stderr:', stderr);
                vscode_1.window.showInformationMessage(`Added Lambda Function '${functionName}:${alias}' permissions to API Gateway.`);
            });
        }
    });
}
exports.multiStepAddLambdaFunctionPermissions = multiStepAddLambdaFunctionPermissions;
function multiStepRemoveLambdaFunctionPermissions(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Remove AWS Lambda Function Permissions for API Gateway';
        const numSteps = 3;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const statementID = vscode.workspace.getConfiguration().get('conf.view.awsStatementID');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (profile.length > 0)
                    settingsReducedSteps++;
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputLambdaFunctionName(input, state));
                return state;
            });
        }
        function inputLambdaFunctionName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.functionName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.functionName === 'string' ? state.functionName : '',
                    prompt: 'Enter the name of an existing Lambda function',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        /*
        async function inputAlias(input: MultiStepInput, state: Partial<State>) {
            
            state.functionAlias = await input.showInputBox({
                title,
                step: 2,
                totalSteps: numSteps-settingsReducedSteps,
                value: state.functionAlias || '',
                prompt: 'Enter the name of function alias to remove permission from',
                validate: validateNameIsUnique,
                shouldResume: shouldResume
            });
            
            if(statementID.length>0){
                
                if(region.length>0){
                    if(profile.length>0){
                    
                    }
                    else{
                        return (input: MultiStepInput) => inputProfile(input, state);
                    }
                }
                else{
                    return (input: MultiStepInput) => pickRegion(input, state);
                }
            
            }
            else{
                return (input: MultiStepInput) => inputStatementID(input, state);
            }
            
        }
        
        */
        /*
        async function inputStatementID(input: MultiStepInput, state: Partial<State>) {
            let currentStep = 3;
            if(settingsReducedSteps>0){
                currentStep = currentStep - settingsReducedSteps + 3;
            }
            state.statementID = await input.showInputBox({
                title,
                step: currentStep,
                totalSteps: numSteps-settingsReducedSteps,
                value: state.statementID || '',
                prompt: 'Enter a Statement ID',
                validate: validateNameIsUnique,
                shouldResume: shouldResume
            });
            
            
            if(region.length>0){
                if(profile.length>0){
                
                }
                else{
                    return (input: MultiStepInput) => inputProfile(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => pickRegion(input, state);
            }
        
            
        }
        */
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined /* TODO: token */);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
                if (profile.length > 0) {
                }
                else {
                    return (input) => inputProfile(input, state);
                }
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 3,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.profile || '',
                    prompt: 'Select an AWS profile (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRuntimes(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['python3.6']
                    .map(label => ({ label }));
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        const util = require('util');
        const exec = util.promisify(require('child_process').exec);
        let cliFunctionName = '';
        cliFunctionName = state.functionName;
        let cliAccountID = '';
        let cliProfile = '';
        if (profile.length > 0)
            cliProfile = profile;
        else
            cliProfile = state.profile;
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        let cliStatementID = '';
        if (statementID.length > 0)
            cliStatementID = statementID;
        else
            cliStatementID = state.statementID;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec('aws sts get-caller-identity --profile ' + profile);
                //console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                const response = JSON.parse(stdout);
                const accountID = response.Account;
                cliAccountID = String(accountID);
                let cliFunctionAlias = 'Production';
                let cliStatementIDD = cliStatementID + '-1';
                let cliCommand = 'aws lambda remove-permission  --function-name "arn:aws:lambda:' +
                    cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName + ':' + cliFunctionAlias + '"' +
                    ' --statement-id ' + cliStatementIDD;
                cliCommand += ' --region ' + cliRegion;
                cliCommand += ' --profile ' + cliProfile;
                vscode_1.window.showInformationMessage(`Removing Lambda Function '${cliFunctionName}:${cliFunctionAlias}' permissions with ${cliCommand}`);
                addLambdaFunctionPermission(cliCommand, cliFunctionName, cliFunctionAlias);
                cliFunctionAlias = 'Development';
                cliStatementIDD = cliStatementID + '-2';
                cliCommand = 'aws lambda remove-permission  --function-name "arn:aws:lambda:' +
                    cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName + ':' + cliFunctionAlias + '"' +
                    ' --statement-id ' + cliStatementIDD;
                cliCommand += ' --region ' + cliRegion;
                cliCommand += ' --profile ' + cliProfile;
                vscode_1.window.showInformationMessage(`Removing Lambda Function '${cliFunctionName}:${cliFunctionAlias}' permissions with ${cliCommand}`);
                addLambdaFunctionPermission(cliCommand, cliFunctionName, cliFunctionAlias);
                return accountID;
            });
        }
        function addLambdaFunctionPermission(cmd, functionName, alias) {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec(cmd);
                console.log('stdout:', stdout);
                console.log('stderr:', stderr);
                vscode_1.window.showInformationMessage(`Removed Lambda Function '${functionName}:${alias}' permissions to API Gateway.`);
            });
        }
    });
}
exports.multiStepRemoveLambdaFunctionPermissions = multiStepRemoveLambdaFunctionPermissions;
function multiStepCreateLambdaFunction(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Create AWS Lambda Function';
        const numSteps = 9;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const accountID = vscode.workspace.getConfiguration().get('conf.view.awsAccountID');
        const executionRole = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole');
        const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime');
        const boilerplateS3Bucket = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Bucket');
        const boilerplateS3Key = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Key');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory');
        const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout');
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet');
        const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup');
        const layers = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers');
        class MyButton {
            constructor(iconPath, tooltip) {
                this.iconPath = iconPath;
                this.tooltip = tooltip;
            }
        }
        const createResourceGroupButton = new MyButton({
            dark: vscode_1.Uri.file(context.asAbsolutePath('resources/dark/add.svg')),
            light: vscode_1.Uri.file(context.asAbsolutePath('resources/light/add.svg')),
        }, 'Create Resource Group');
        const resourceGroups = ['vscode-data-function', 'vscode-appservice-microservices', 'vscode-appservice-monitor', 'vscode-appservice-preview', 'vscode-appservice-prod']
            .map(label => ({ label }));
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (profile.length > 0) {
                    settingsReducedSteps++;
                }
                if (executionRole.length > 0) {
                    settingsReducedSteps++;
                }
                if (runtime.length > 0) {
                    settingsReducedSteps++;
                }
                if (boilerplateS3Bucket.length > 0) {
                    settingsReducedSteps++;
                }
                if (boilerplateS3Key.length > 0) {
                    settingsReducedSteps++;
                }
                if (region.length > 0) {
                    settingsReducedSteps++;
                }
                if (layers.length > 0) {
                    settingsReducedSteps++;
                    settingsReducedSteps++;
                }
                /*
                await MultiStepInput.run(input => pickResourceGroup(input, state));
                */
                yield MultiStepInput.run(input => inputLambdaFunctionName(input, state));
                return state;
            });
        }
        /*
        async function pickResourceGroup(input: MultiStepInput, state: Partial<State>) {
            const pick = await input.showQuickPick({
                title,
                step: 1,
                totalSteps: 3,
                placeholder: 'Pick a resource group',
                items: resourceGroups,
                activeItem: typeof state.resourceGroup !== 'string' ? state.resourceGroup : undefined,
                buttons: [createResourceGroupButton],
                shouldResume: shouldResume
            });
            if (pick instanceof MyButton) {
                return (input: MultiStepInput) => inputResourceGroupName(input, state);
            }
            state.resourceGroup = pick;
            return (input: MultiStepInput) => inputName(input, state);
        }
        */
        function inputLambdaFunctionName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.functionName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.functionName === 'string' ? state.functionName : '',
                    prompt: 'Choose a unique name for the lambda function',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (runtime.length > 0) {
                    if (executionRole.length > 0) {
                        if (region.length > 0) {
                            if (profile.length > 0) {
                                if (layers.length > 0) {
                                }
                                else {
                                    return (input) => inputLayer(input, state);
                                }
                            }
                            else {
                                return (input) => inputProfile(input, state);
                            }
                        }
                        else {
                            return (input) => pickRegion(input, state);
                        }
                    }
                    else {
                        return (input) => inputRole(input, state);
                    }
                }
                else {
                    return (input) => pickRuntime(input, state);
                }
            });
        }
        function pickRuntime(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                /*
                const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
                */
                const runtimes = yield getAvailableRuntimes(state.runtime, undefined /* TODO: token */);
                state.runtime = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a runtime',
                    items: runtimes,
                    activeItem: state.runtime,
                    shouldResume: shouldResume
                });
                if (executionRole.length > 0) {
                    if (region.length > 0) {
                        if (profile.length > 0) {
                            return (input) => inputLayer(input, state);
                        }
                        else {
                            return (input) => inputProfile(input, state);
                        }
                    }
                    else {
                        return (input) => pickRegion(input, state);
                    }
                }
                else {
                    return (input) => inputRole(input, state);
                }
            });
        }
        function inputRole(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const additionalSteps = typeof state.functionName === 'string' ? 1 : 0;
                // TODO: Remember current value when navigating back.
                state.role = yield input.showInputBox({
                    title,
                    step: 3,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.role || '',
                    prompt: 'Select an execution role',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                    if (profile.length > 0) {
                        return (input) => inputLayer(input, state);
                    }
                    else {
                        return (input) => inputProfile(input, state);
                    }
                }
                else {
                    return (input) => pickRegion(input, state);
                }
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                /*
                const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
                */
                const regions = yield getAvailableRegions(state.region, undefined /* TODO: token */);
                // TODO: Remember currently active item when navigating back.
                state.region = yield input.showQuickPick({
                    title,
                    step: 4,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
                if (profile.length > 0) {
                    return (input) => inputLayer(input, state);
                }
                else {
                    return (input) => inputProfile(input, state);
                }
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 5,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.profile || '',
                    prompt: 'Select an AWS profile (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputLayer(input, state);
            });
        }
        //merge to this layer
        function inputLayer(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 6;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 2;
                }
                state.layer = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.layer || '',
                    prompt: 'Pick a layer to add to your function (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputLayerVersion(input, state);
            });
        }
        function inputLayerVersion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 7;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 2;
                }
                state.layerVersion = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.layerVersion || '',
                    prompt: 'Pick a layer version',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (boilerplateS3Bucket.length > 0) {
                    if (boilerplateS3Key.length > 0) {
                    }
                    else {
                        return (input) => inputS3Key(input, state);
                    }
                }
                else {
                    return (input) => inputS3Bucket(input, state);
                }
            });
        }
        function inputS3Bucket(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.s3Bucket = yield input.showInputBox({
                    title,
                    step: 8,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.s3Bucket || '',
                    prompt: 'Pick a S3 Bucket',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (boilerplateS3Key.length > 0) {
                }
                else {
                    return (input) => inputS3Key(input, state);
                }
            });
        }
        function inputS3Key(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.s3Key = yield input.showInputBox({
                    title,
                    step: 9,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.s3Key || '',
                    prompt: 'Enter a S3 Key',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                /*
                return (input: MultiStepInput) => pickRegion(input, state);
                */
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRuntimes(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['python3.6']
                    .map(label => ({ label }));
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        const util = require('util');
        //const execSync = util.promisify(require('child_process').execSync);
        const execSync = require('child_process').execSync;
        const exec = util.promisify(require('child_process').exec);
        let cliFunctionName = '';
        cliFunctionName = state.functionName;
        let cliAccountID = '';
        let cliProfile = '';
        if (profile.length > 0)
            cliProfile = profile;
        else
            cliProfile = state.profile;
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        let cliRuntime = '';
        if (runtime.length > 0)
            cliRuntime = runtime;
        else
            cliRuntime = state.runtime.label;
        let cliExecutionRole = '';
        if (executionRole.length > 0)
            cliExecutionRole = executionRole;
        else
            cliExecutionRole = state.role;
        let cliS3Bucket = '';
        if (boilerplateS3Bucket.length > 0)
            cliS3Bucket = boilerplateS3Bucket;
        else
            cliS3Bucket = state.s3Bucket;
        let cliS3Key = '';
        if (boilerplateS3Key.length > 0)
            cliS3Key = boilerplateS3Key;
        else
            cliS3Key = state.s3Key;
        let cliLayer = '';
        cliLayer = state.layer;
        let cliLayerVersion = '';
        cliLayerVersion = state.layerVersion;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                //get accountID first with CLI
                const cmd = 'aws sts get-caller-identity --profile ' + cliProfile;
                //const { stdout, stderr } = execSync(cmd);
                const { stdout, stderr } = yield exec(cmd);
                //console.log(cmd);
                console.log('stdout1:', stdout);
                console.log('stderr2:', stderr);
                const response = JSON.parse(stdout);
                const accountID = response.Account;
                cliAccountID = String(accountID);
                let cliCommand = 'aws lambda create-function --function-name "arn:aws:lambda:' +
                    cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName + '" --runtime "' +
                    cliRuntime + '" --role "arn:aws:iam::' + cliAccountID + ':role/' + cliExecutionRole +
                    '" --handler "lambda_function.lambda_handler" --code "S3Bucket=' + cliS3Bucket +
                    ',S3Key=' + cliS3Key + '"';
                //cliCommand += ' --layer "arn:aws:lambda:'+cliRegion+':'+cliAccountID+':layer:'+cliLayer+':'+cliLayerVersion+'"';
                cliCommand += ' --memory-size ' + memory;
                cliCommand += ' --timeout ' + timeout;
                cliCommand += ' --vpc-config SubnetIds=' + subnet + ',SecurityGroupIds=' + securityGroup;
                let layerList = layers.split(',');
                let layerPrefix = 'arn:aws:lambda:' + region + ':' + accountID + ':layer:';
                let layerString = '';
                for (let i = 0; i < layerList.length; i++) {
                    layerString += '"' + layerPrefix + layerList[i] + '"';
                    if (i < layerList.length - 1)
                        layerString += ' ';
                }
                cliCommand += ' --layers ' + layerString;
                cliCommand += ' --region ' + cliRegion;
                cliCommand += ' --profile ' + cliProfile;
                console.log(cliCommand);
                vscode_1.window.showInformationMessage(`Creating Lambda Function '${cliFunctionName}' with command ${cliCommand}`);
                //create aliases for lambda function
                buildLambdaFunction(cliCommand);
                return accountID;
            });
        }
        /*
        async function executeCliCommand(cmd:string) {
            const { stdout, stderr } = await exec(cmd);
            console.log('stdout:', stdout);
            console.log('stderr:', stderr);
        }
        */
        function aliasCommand(cmd) {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec(cmd);
                console.log('stdout5:', stdout);
                console.log('stderr6:', stderr);
            });
        }
        function buildLambdaFunction(cmd) {
            let lambdaAlias = ['Development', 'Production'];
            const { stdout, stderr } = execSync(cmd);
            console.log('stdout3:', stdout);
            console.log('stderr4:', stderr);
            //generate aliases only after main function is synchronously created
            for (let i = 0; i < lambdaAlias.length; i++) {
                let cliAliasCommand = 'aws lambda create-alias --function-name "arn:aws:lambda:' +
                    cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName +
                    '" --name ' + lambdaAlias[i] + ' --function-version $LATEST';
                cliAliasCommand += ' --region ' + cliRegion;
                cliAliasCommand += ' --profile ' + cliProfile;
                vscode_1.window.showInformationMessage(`Creating Lambda Function Alias ${lambdaAlias[i]} for '${cliFunctionName}' with command ${cliAliasCommand}`);
                aliasCommand(cliAliasCommand);
                vscode_1.window.showInformationMessage(`Created Lambda Function Alias ${lambdaAlias[i]} for '${cliFunctionName}'`);
            }
        }
    });
}
exports.multiStepCreateLambdaFunction = multiStepCreateLambdaFunction;
function multiStepUpdateLambdaFunction(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS Lambda Function';
        const numSteps = 9;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const accountID = vscode.workspace.getConfiguration().get('conf.view.awsAccountID');
        const executionRole = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole');
        const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime');
        const boilerplateS3Bucket = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Bucket');
        const boilerplateS3Key = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Key');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory');
        const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout');
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet');
        const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup');
        const layers = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers');
        class MyButton {
            constructor(iconPath, tooltip) {
                this.iconPath = iconPath;
                this.tooltip = tooltip;
            }
        }
        const createResourceGroupButton = new MyButton({
            dark: vscode_1.Uri.file(context.asAbsolutePath('resources/dark/add.svg')),
            light: vscode_1.Uri.file(context.asAbsolutePath('resources/light/add.svg')),
        }, 'Create Resource Group');
        const resourceGroups = ['vscode-data-function', 'vscode-appservice-microservices', 'vscode-appservice-monitor', 'vscode-appservice-preview', 'vscode-appservice-prod']
            .map(label => ({ label }));
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (profile.length > 0) {
                    settingsReducedSteps++;
                }
                if (executionRole.length > 0) {
                    settingsReducedSteps++;
                }
                if (runtime.length > 0) {
                    settingsReducedSteps++;
                }
                if (boilerplateS3Bucket.length > 0) {
                    settingsReducedSteps++;
                }
                if (boilerplateS3Key.length > 0) {
                    settingsReducedSteps++;
                }
                if (region.length > 0) {
                    settingsReducedSteps++;
                }
                if (layers.length > 0) {
                    settingsReducedSteps++;
                    settingsReducedSteps++;
                }
                /*
                await MultiStepInput.run(input => pickResourceGroup(input, state));
                */
                yield MultiStepInput.run(input => inputLambdaFunctionName(input, state));
                return state;
            });
        }
        /*
        async function pickResourceGroup(input: MultiStepInput, state: Partial<State>) {
            const pick = await input.showQuickPick({
                title,
                step: 1,
                totalSteps: 3,
                placeholder: 'Pick a resource group',
                items: resourceGroups,
                activeItem: typeof state.resourceGroup !== 'string' ? state.resourceGroup : undefined,
                buttons: [createResourceGroupButton],
                shouldResume: shouldResume
            });
            if (pick instanceof MyButton) {
                return (input: MultiStepInput) => inputResourceGroupName(input, state);
            }
            state.resourceGroup = pick;
            return (input: MultiStepInput) => inputName(input, state);
        }
        */
        function inputLambdaFunctionName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.functionName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.functionName === 'string' ? state.functionName : '',
                    prompt: 'Enter the name of an existing lambda function',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (runtime.length > 0) {
                    if (executionRole.length > 0) {
                        if (region.length > 0) {
                            if (profile.length > 0) {
                                if (layers.length > 0) {
                                }
                                else {
                                    return (input) => inputLayer(input, state);
                                }
                            }
                            else {
                                return (input) => inputProfile(input, state);
                            }
                        }
                        else {
                            return (input) => pickRegion(input, state);
                        }
                    }
                    else {
                        return (input) => inputRole(input, state);
                    }
                }
                else {
                    return (input) => pickRuntime(input, state);
                }
            });
        }
        function pickRuntime(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                /*
                const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
                */
                const runtimes = yield getAvailableRuntimes(state.runtime, undefined /* TODO: token */);
                state.runtime = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a runtime',
                    items: runtimes,
                    activeItem: state.runtime,
                    shouldResume: shouldResume
                });
                if (executionRole.length > 0) {
                    if (region.length > 0) {
                        if (profile.length > 0) {
                            return (input) => inputLayer(input, state);
                        }
                        else {
                            return (input) => inputProfile(input, state);
                        }
                    }
                    else {
                        return (input) => pickRegion(input, state);
                    }
                }
                else {
                    return (input) => inputRole(input, state);
                }
            });
        }
        function inputRole(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const additionalSteps = typeof state.functionName === 'string' ? 1 : 0;
                // TODO: Remember current value when navigating back.
                state.role = yield input.showInputBox({
                    title,
                    step: 3,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.role || '',
                    prompt: 'Select an execution role',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                    if (profile.length > 0) {
                        return (input) => inputLayer(input, state);
                    }
                    else {
                        return (input) => inputProfile(input, state);
                    }
                }
                else {
                    return (input) => pickRegion(input, state);
                }
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                /*
                const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
                */
                const regions = yield getAvailableRegions(state.region, undefined /* TODO: token */);
                // TODO: Remember currently active item when navigating back.
                state.region = yield input.showQuickPick({
                    title,
                    step: 4,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
                if (profile.length > 0) {
                    return (input) => inputLayer(input, state);
                }
                else {
                    return (input) => inputProfile(input, state);
                }
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 5,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.profile || '',
                    prompt: 'Select an AWS profile (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputLayer(input, state);
            });
        }
        //merge to this layer
        function inputLayer(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 6;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 2;
                }
                state.layer = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.layer || '',
                    prompt: 'Pick a layer to add to your function (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputLayerVersion(input, state);
            });
        }
        function inputLayerVersion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 7;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 2;
                }
                state.layerVersion = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.layerVersion || '',
                    prompt: 'Pick a layer version',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (boilerplateS3Bucket.length > 0) {
                    if (boilerplateS3Key.length > 0) {
                    }
                    else {
                        return (input) => inputS3Key(input, state);
                    }
                }
                else {
                    return (input) => inputS3Bucket(input, state);
                }
            });
        }
        function inputS3Bucket(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.s3Bucket = yield input.showInputBox({
                    title,
                    step: 8,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.s3Bucket || '',
                    prompt: 'Pick a S3 Bucket',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (boilerplateS3Key.length > 0) {
                }
                else {
                    return (input) => inputS3Key(input, state);
                }
            });
        }
        function inputS3Key(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.s3Key = yield input.showInputBox({
                    title,
                    step: 9,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.s3Key || '',
                    prompt: 'Enter a S3 Key',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                /*
                return (input: MultiStepInput) => pickRegion(input, state);
                */
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRuntimes(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['python3.6']
                    .map(label => ({ label }));
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        const util = require('util');
        //const execSync = util.promisify(require('child_process').execSync);
        const execSync = require('child_process').execSync;
        const exec = util.promisify(require('child_process').exec);
        let cliFunctionName = '';
        cliFunctionName = state.functionName;
        let cliAccountID = '';
        let cliProfile = '';
        if (profile.length > 0)
            cliProfile = profile;
        else
            cliProfile = state.profile;
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        let cliRuntime = '';
        if (runtime.length > 0)
            cliRuntime = runtime;
        else
            cliRuntime = state.runtime.label;
        let cliExecutionRole = '';
        if (executionRole.length > 0)
            cliExecutionRole = executionRole;
        else
            cliExecutionRole = state.role;
        let cliS3Bucket = '';
        if (boilerplateS3Bucket.length > 0)
            cliS3Bucket = boilerplateS3Bucket;
        else
            cliS3Bucket = state.s3Bucket;
        let cliS3Key = '';
        if (boilerplateS3Key.length > 0)
            cliS3Key = boilerplateS3Key;
        else
            cliS3Key = state.s3Key;
        let cliLayer = '';
        cliLayer = state.layer;
        let cliLayerVersion = '';
        cliLayerVersion = state.layerVersion;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                //get accountID first with CLI
                const cmd = 'aws sts get-caller-identity --profile ' + cliProfile;
                //const { stdout, stderr } = execSync(cmd);
                const { stdout, stderr } = yield exec(cmd);
                //console.log(cmd);
                console.log('stdout1:', stdout);
                console.log('stderr2:', stderr);
                const response = JSON.parse(stdout);
                const accountID = response.Account;
                cliAccountID = String(accountID);
                let cliCommand = 'aws lambda update-function-configuration --function-name "arn:aws:lambda:' +
                    cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName + '" --runtime "' +
                    cliRuntime + '" --role "arn:aws:iam::' + cliAccountID + ':role/' + cliExecutionRole + '"';
                //cliCommand += ' --layer "arn:aws:lambda:'+cliRegion+':'+cliAccountID+':layer:'+cliLayer+':'+cliLayerVersion+'"';
                cliCommand += ' --memory-size ' + memory;
                cliCommand += ' --timeout ' + timeout;
                cliCommand += ' --vpc-config SubnetIds=' + subnet + ',SecurityGroupIds=' + securityGroup;
                let layerList = layers.split(',');
                let layerPrefix = 'arn:aws:lambda:' + region + ':' + accountID + ':layer:';
                let layerString = '';
                for (let i = 0; i < layerList.length; i++) {
                    layerString += '"' + layerPrefix + layerList[i] + '"';
                    if (i < layerList.length - 1)
                        layerString += ' ';
                }
                cliCommand += ' --layers ' + layerString;
                cliCommand += ' --region ' + cliRegion;
                cliCommand += ' --profile ' + cliProfile;
                console.log(cliCommand);
                vscode_1.window.showInformationMessage(`Updating Lambda Function '${cliFunctionName}' with command ${cliCommand}`);
                const { stdout3, stderr4 } = execSync(cliCommand);
                console.log('stdout3:', stdout3);
                console.log('stderr4:', stderr4);
                vscode_1.window.showInformationMessage(`Updated Lambda Function '${cliFunctionName}'`);
                return accountID;
            });
        }
    });
}
exports.multiStepUpdateLambdaFunction = multiStepUpdateLambdaFunction;
function multiStepCreateLambdaFunctionCRUD(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Create AWS Lambda CRUD Functions';
        const numSteps = 9;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const executionRole = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole');
        const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime');
        const boilerplateS3Bucket = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Bucket');
        const boilerplateS3Key = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Key');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory');
        const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout');
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet');
        const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup');
        const layers = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers');
        class MyButton {
            constructor(iconPath, tooltip) {
                this.iconPath = iconPath;
                this.tooltip = tooltip;
            }
        }
        const createResourceGroupButton = new MyButton({
            dark: vscode_1.Uri.file(context.asAbsolutePath('resources/dark/add.svg')),
            light: vscode_1.Uri.file(context.asAbsolutePath('resources/light/add.svg')),
        }, 'Create Resource Group');
        const resourceGroups = ['vscode-data-function', 'vscode-appservice-microservices', 'vscode-appservice-monitor', 'vscode-appservice-preview', 'vscode-appservice-prod']
            .map(label => ({ label }));
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (profile.length > 0) {
                    settingsReducedSteps++;
                }
                if (executionRole.length > 0) {
                    settingsReducedSteps++;
                }
                if (runtime.length > 0) {
                    settingsReducedSteps++;
                }
                if (boilerplateS3Bucket.length > 0) {
                    settingsReducedSteps++;
                }
                if (boilerplateS3Key.length > 0) {
                    settingsReducedSteps++;
                }
                if (region.length > 0) {
                    settingsReducedSteps++;
                }
                if (layers.length > 0) {
                    settingsReducedSteps++;
                    settingsReducedSteps++;
                }
                yield MultiStepInput.run(input => inputLambdaFunctionBaseName(input, state));
                return state;
            });
        }
        function inputLambdaFunctionBaseName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.functionBaseName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.functionBaseName === 'string' ? state.functionBaseName : '',
                    prompt: 'Choose a unique base name for the lambda function',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (runtime.length > 0) {
                    if (executionRole.length > 0) {
                        if (region.length > 0) {
                            if (profile.length > 0) {
                                if (layers.length > 0) {
                                }
                                else {
                                    return (input) => inputLayer(input, state);
                                }
                            }
                            else {
                                return (input) => inputProfile(input, state);
                            }
                        }
                        else {
                            return (input) => pickRegion(input, state);
                        }
                    }
                    else {
                        return (input) => inputRole(input, state);
                    }
                }
                else {
                    return (input) => pickRuntime(input, state);
                }
            });
        }
        function pickRuntime(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                /*
                const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
                */
                const runtimes = yield getAvailableRuntimes(state.runtime, undefined /* TODO: token */);
                state.runtime = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a runtime',
                    items: runtimes,
                    activeItem: state.runtime,
                    shouldResume: shouldResume
                });
                if (executionRole.length > 0) {
                    if (region.length > 0) {
                        if (profile.length > 0) {
                            return (input) => inputLayer(input, state);
                        }
                        else {
                            return (input) => inputProfile(input, state);
                        }
                    }
                    else {
                        return (input) => pickRegion(input, state);
                    }
                }
                else {
                    return (input) => inputRole(input, state);
                }
            });
        }
        function inputRole(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.role = yield input.showInputBox({
                    title,
                    step: 3,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.role || '',
                    prompt: 'Select an execution role',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                    if (profile.length > 0) {
                        return (input) => inputLayer(input, state);
                    }
                    else {
                        return (input) => inputProfile(input, state);
                    }
                }
                else {
                    return (input) => pickRegion(input, state);
                }
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined /* TODO: token */);
                state.region = yield input.showQuickPick({
                    title,
                    step: 4,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
                if (profile.length > 0) {
                    return (input) => inputLayer(input, state);
                }
                else {
                    return (input) => inputProfile(input, state);
                }
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 5,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.profile || '',
                    prompt: 'Select an AWS profile (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputLayer(input, state);
            });
        }
        //merge to this layer
        function inputLayer(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 6;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 2;
                }
                state.layer = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.layer || '',
                    prompt: 'Pick a layer to add to your function (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputLayerVersion(input, state);
            });
        }
        function inputLayerVersion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 7;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 2;
                }
                state.layerVersion = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.layerVersion || '',
                    prompt: 'Pick a layer version',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (boilerplateS3Bucket.length > 0) {
                    if (boilerplateS3Key.length > 0) {
                    }
                    else {
                        return (input) => inputS3Key(input, state);
                    }
                }
                else {
                    return (input) => inputS3Bucket(input, state);
                }
            });
        }
        function inputS3Bucket(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.s3Bucket = yield input.showInputBox({
                    title,
                    step: 8,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.s3Bucket || '',
                    prompt: 'Pick a S3 Bucket',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (boilerplateS3Key.length > 0) {
                }
                else {
                    return (input) => inputS3Key(input, state);
                }
            });
        }
        function inputS3Key(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.s3Key = yield input.showInputBox({
                    title,
                    step: 9,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.s3Key || '',
                    prompt: 'Enter a S3 Key',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                /*
                return (input: MultiStepInput) => pickRegion(input, state);
                */
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRuntimes(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['python3.6']
                    .map(label => ({ label }));
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        const util = require('util');
        const execSync = util.promisify(require('child_process').execSync);
        const exec = util.promisify(require('child_process').exec);
        let cliFunctionBaseName = '';
        cliFunctionBaseName = state.functionBaseName;
        let cliFunctionName = '';
        let cliAccountID = '';
        let cliProfile = '';
        if (profile.length > 0)
            cliProfile = profile;
        else
            cliProfile = state.profile;
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        let cliRuntime = '';
        if (runtime.length > 0)
            cliRuntime = runtime;
        else
            cliRuntime = state.runtime.label;
        let cliExecutionRole = '';
        if (executionRole.length > 0)
            cliExecutionRole = executionRole;
        else
            cliExecutionRole = state.role;
        let cliS3Bucket = '';
        if (boilerplateS3Bucket.length > 0)
            cliS3Bucket = boilerplateS3Bucket;
        else
            cliS3Bucket = state.s3Bucket;
        let cliS3Key = '';
        if (boilerplateS3Key.length > 0)
            cliS3Key = boilerplateS3Key;
        else
            cliS3Key = state.s3Key;
        let cliLayer = '';
        cliLayer = state.layer;
        let cliLayerVersion = '';
        cliLayerVersion = state.layerVersion;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec('aws sts get-caller-identity --profile ' + cliProfile);
                //console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                const response = JSON.parse(stdout);
                const accountID = response.Account;
                cliAccountID = String(accountID);
                let cliCommand = '';
                let crudPrefix = ['Add', 'Update', 'Query', 'Get', 'Bulk', 'Empty', 'Delete'];
                for (let i = 0; i < crudPrefix.length; i++) {
                    cliFunctionName = crudPrefix[i] + cliFunctionBaseName;
                    cliCommand = 'aws lambda create-function  --function-name "arn:aws:lambda:' +
                        cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName + '" --runtime "' +
                        cliRuntime + '" --role "arn:aws:iam::' + cliAccountID + ':role/' + cliExecutionRole +
                        '" --handler "lambda_function.lambda_handler" --code "S3Bucket=' + cliS3Bucket +
                        ',S3Key=' + cliS3Key + '"';
                    //cliCommand += ' --layer "arn:aws:lambda:'+cliRegion+':'+cliAccountID+':layer:'+cliLayer+':'+cliLayerVersion+'"';
                    cliCommand += ' --memory-size ' + memory;
                    cliCommand += ' --timeout ' + timeout;
                    cliCommand += ' --vpc-config SubnetIds=' + subnet + ',SecurityGroupIds=' + securityGroup;
                    let layerList = layers.split(',');
                    let layerPrefix = 'arn:aws:lambda:' + region + ':' + accountID + ':layer:';
                    let layerString = '';
                    for (let i = 0; i < layerList.length; i++) {
                        layerString += '"' + layerPrefix + layerList[i] + '"';
                        if (i < layerList.length - 1)
                            layerString += ' ';
                    }
                    cliCommand += ' --layers ' + layerString;
                    cliCommand += ' --region ' + cliRegion;
                    cliCommand += ' --profile ' + cliProfile;
                    vscode_1.window.showInformationMessage(`Creating Lambda Function '${cliFunctionName}' with command ${cliCommand}`);
                    buildLambdaFunction(cliCommand);
                }
                return accountID;
            });
        }
        function aliasCommand(cmd) {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec(cmd);
                console.log('stdout:', stdout);
                console.log('stderr:', stderr);
            });
        }
        function buildLambdaFunction(cmd) {
            let lambdaAlias = ['Development', 'Production'];
            const { stdout, stderr } = execSync(cmd);
            console.log('stdout:', stdout);
            console.log('stderr:', stderr);
            vscode_1.window.showInformationMessage(`Created Lambda Function '${cliFunctionName}'`);
            //generate aliases only after main function is synchronously created
            for (let i = 0; i < lambdaAlias.length; i++) {
                let cliAliasCommand = 'aws lambda create-alias --function-name "arn:aws:lambda:' +
                    cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName +
                    '" --name ' + lambdaAlias[i] + ' --function-version $LATEST';
                cliAliasCommand += ' --region ' + cliRegion;
                cliAliasCommand += ' --profile ' + cliProfile;
                vscode_1.window.showInformationMessage(`Creating Lambda Function Alias ${lambdaAlias[i]} for '${cliFunctionName}' with command ${cliAliasCommand}`);
                aliasCommand(cliAliasCommand);
                vscode_1.window.showInformationMessage(`Created Lambda Function Alias ${lambdaAlias[i]} for '${cliFunctionName}'`);
            }
        }
    });
}
exports.multiStepCreateLambdaFunctionCRUD = multiStepCreateLambdaFunctionCRUD;
function multiStepUpdateLambdaFunctionCRUD(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS Lambda CRUD Functions';
        const numSteps = 7;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const executionRole = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole');
        const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory');
        const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout');
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet');
        const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup');
        const layers = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers');
        class MyButton {
            constructor(iconPath, tooltip) {
                this.iconPath = iconPath;
                this.tooltip = tooltip;
            }
        }
        const createResourceGroupButton = new MyButton({
            dark: vscode_1.Uri.file(context.asAbsolutePath('resources/dark/add.svg')),
            light: vscode_1.Uri.file(context.asAbsolutePath('resources/light/add.svg')),
        }, 'Create Resource Group');
        const resourceGroups = ['vscode-data-function', 'vscode-appservice-microservices', 'vscode-appservice-monitor', 'vscode-appservice-preview', 'vscode-appservice-prod']
            .map(label => ({ label }));
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (profile.length > 0) {
                    settingsReducedSteps++;
                }
                if (executionRole.length > 0) {
                    settingsReducedSteps++;
                }
                if (runtime.length > 0) {
                    settingsReducedSteps++;
                }
                if (region.length > 0) {
                    settingsReducedSteps++;
                }
                if (layers.length > 0) {
                    settingsReducedSteps++;
                    settingsReducedSteps++;
                }
                yield MultiStepInput.run(input => inputLambdaFunctionBaseName(input, state));
                return state;
            });
        }
        function inputLambdaFunctionBaseName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.functionBaseName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.functionBaseName === 'string' ? state.functionBaseName : '',
                    prompt: 'Choose an existing base name for the lambda functions to update',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (runtime.length > 0) {
                    if (executionRole.length > 0) {
                        if (region.length > 0) {
                            if (profile.length > 0) {
                                if (layers.length > 0) {
                                }
                                else {
                                    return (input) => inputLayer(input, state);
                                }
                            }
                            else {
                                return (input) => inputProfile(input, state);
                            }
                        }
                        else {
                            return (input) => pickRegion(input, state);
                        }
                    }
                    else {
                        return (input) => inputRole(input, state);
                    }
                }
                else {
                    return (input) => pickRuntime(input, state);
                }
            });
        }
        function pickRuntime(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                /*
                const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
                */
                const runtimes = yield getAvailableRuntimes(state.runtime, undefined /* TODO: token */);
                state.runtime = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a runtime',
                    items: runtimes,
                    activeItem: state.runtime,
                    shouldResume: shouldResume
                });
                if (executionRole.length > 0) {
                    if (region.length > 0) {
                        if (profile.length > 0) {
                            return (input) => inputLayer(input, state);
                        }
                        else {
                            return (input) => inputProfile(input, state);
                        }
                    }
                    else {
                        return (input) => pickRegion(input, state);
                    }
                }
                else {
                    return (input) => inputRole(input, state);
                }
            });
        }
        function inputRole(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.role = yield input.showInputBox({
                    title,
                    step: 3,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.role || '',
                    prompt: 'Select an execution role',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                    if (profile.length > 0) {
                        return (input) => inputLayer(input, state);
                    }
                    else {
                        return (input) => inputProfile(input, state);
                    }
                }
                else {
                    return (input) => pickRegion(input, state);
                }
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined /* TODO: token */);
                state.region = yield input.showQuickPick({
                    title,
                    step: 4,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
                if (profile.length > 0) {
                    return (input) => inputLayer(input, state);
                }
                else {
                    return (input) => inputProfile(input, state);
                }
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 5,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.profile || '',
                    prompt: 'Select an AWS profile (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputLayer(input, state);
            });
        }
        //merge to this layer
        function inputLayer(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 6;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 2;
                }
                state.layer = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.layer || '',
                    prompt: 'Pick a layer to add to your function (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                return (input) => inputLayerVersion(input, state);
            });
        }
        function inputLayerVersion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                let currentStep = 7;
                if (settingsReducedSteps > 0) {
                    currentStep = currentStep - settingsReducedSteps + 2;
                }
                state.layerVersion = yield input.showInputBox({
                    title,
                    step: currentStep,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.layerVersion || '',
                    prompt: 'Pick a layer version',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRuntimes(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['python3.6']
                    .map(label => ({ label }));
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        const util = require('util');
        const execSync = util.promisify(require('child_process').execSync);
        const exec = util.promisify(require('child_process').exec);
        let cliFunctionBaseName = '';
        cliFunctionBaseName = state.functionBaseName;
        let cliFunctionName = '';
        let cliAccountID = '';
        let cliProfile = '';
        if (profile.length > 0)
            cliProfile = profile;
        else
            cliProfile = state.profile;
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        let cliRuntime = '';
        if (runtime.length > 0)
            cliRuntime = runtime;
        else
            cliRuntime = state.runtime.label;
        let cliExecutionRole = '';
        if (executionRole.length > 0)
            cliExecutionRole = executionRole;
        else
            cliExecutionRole = state.role;
        let cliLayer = '';
        cliLayer = state.layer;
        let cliLayerVersion = '';
        cliLayerVersion = state.layerVersion;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec('aws sts get-caller-identity --profile ' + cliProfile);
                //console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                const response = JSON.parse(stdout);
                const accountID = response.Account;
                cliAccountID = String(accountID);
                let cliCommand = '';
                let crudPrefix = ['Add', 'Update', 'Query', 'Get', 'Bulk', 'Empty', 'Delete'];
                for (let i = 0; i < crudPrefix.length; i++) {
                    cliFunctionName = crudPrefix[i] + cliFunctionBaseName;
                    let cliCommand = 'aws lambda update-function-configuration --function-name "arn:aws:lambda:' +
                        cliRegion + ':' + cliAccountID + ':function:' + cliFunctionName + '" --runtime "' +
                        cliRuntime + '" --role "arn:aws:iam::' + cliAccountID + ':role/' + cliExecutionRole + '"';
                    //cliCommand += ' --layer "arn:aws:lambda:'+cliRegion+':'+cliAccountID+':layer:'+cliLayer+':'+cliLayerVersion+'"';
                    cliCommand += ' --memory-size ' + memory;
                    cliCommand += ' --timeout ' + timeout;
                    cliCommand += ' --vpc-config SubnetIds=' + subnet + ',SecurityGroupIds=' + securityGroup;
                    let layerList = layers.split(',');
                    let layerPrefix = 'arn:aws:lambda:' + region + ':' + accountID + ':layer:';
                    let layerString = '';
                    for (let i = 0; i < layerList.length; i++) {
                        layerString += '"' + layerPrefix + layerList[i] + '"';
                        if (i < layerList.length - 1)
                            layerString += ' ';
                    }
                    cliCommand += ' --layers ' + layerString;
                    cliCommand += ' --region ' + cliRegion;
                    cliCommand += ' --profile ' + cliProfile;
                    vscode_1.window.showInformationMessage(`Updating Lambda Function '${cliFunctionName}' with command ${cliCommand}`);
                    updateLambdaFunction(cliCommand);
                }
                return accountID;
            });
        }
        function updateLambdaFunction(cmd) {
            const { stdout, stderr } = execSync(cmd);
            console.log('stdout:', stdout);
            console.log('stderr:', stderr);
            vscode_1.window.showInformationMessage(`Updated Lambda Function '${cliFunctionName}'`);
        }
    });
}
exports.multiStepUpdateLambdaFunctionCRUD = multiStepUpdateLambdaFunctionCRUD;
function multiStepUpdateLambdaLayerVersion(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update Lambda Layer Version';
        const numSteps = 7;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const layerName = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayer');
        const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime');
        const folder = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayerFolder');
        const tempFilename = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayerTempZipFilename');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (profile.length > 0)
                    settingsReducedSteps++;
                if (region.length > 0)
                    settingsReducedSteps++;
                if (layerName.length > 0)
                    settingsReducedSteps++;
                if (runtime.length > 0)
                    settingsReducedSteps++;
                if (folder.length > 0)
                    settingsReducedSteps++;
                if (tempFilename.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputLambdaLayerVersionDescription(input, state));
                return state;
            });
        }
        function inputLambdaLayerVersionDescription(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.description = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.description === 'string' ? state.description : '',
                    prompt: 'Enter a description for the new layer version.',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
                if (profile.length > 0) {
                }
                else {
                    return (input) => inputProfile(input, state);
                }
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 3,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.profile || '',
                    prompt: 'Select an AWS profile (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRuntimes(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['python3.6']
                    .map(label => ({ label }));
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        const util = require('util');
        const exec = util.promisify(require('child_process').exec);
        const execSync = require('child_process').execSync;
        let cliLayerVersionDescription = '';
        cliLayerVersionDescription = state.description;
        //cliFunctionAlias = state.functionAlias;
        let cliProfile = '';
        if (profile.length > 0)
            cliProfile = profile;
        else
            cliProfile = state.profile;
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        let cliLambdaLayerName = '';
        if (layerName.length > 0)
            cliLambdaLayerName = layerName;
        let cliRuntime = '';
        if (runtime.length > 0)
            cliRuntime = runtime;
        let cliLambdaLayerVersionDescription = '';
        if (state.description.length > 0)
            cliLambdaLayerVersionDescription = state.description;
        let cliLambdaLayerFolder = '';
        if (folder.length > 0)
            cliLambdaLayerFolder = folder;
        let cliLambdaLayerTempZipFilename = '';
        if (tempFilename.length > 0)
            cliLambdaLayerTempZipFilename = tempFilename;
        startBuild();
        function delay(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec('aws sts get-caller-identity --profile ' + profile);
                //console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                let cliLambdaLayerTempZipFilenamePath = cliLambdaLayerFolder + '\\..\\' + cliLambdaLayerTempZipFilename;
                let delCommand = 'del ' + cliLambdaLayerTempZipFilenamePath;
                //window.showInformationMessage(delCommand);
                try {
                    const { stdoutA, stderrB } = execSync(delCommand);
                }
                catch (err) {
                    console.log(cliLambdaLayerFolder + '\\..\\' + cliLambdaLayerTempZipFilename + ' does not exist.');
                }
                vscode_1.window.showInformationMessage(`Compressing '${cliLambdaLayerFolder}' into ${cliLambdaLayerTempZipFilenamePath}`);
                //tar zip the folder        
                let zipCommand = 'tar.exe -C "' + cliLambdaLayerFolder + '" -acf "' + cliLambdaLayerTempZipFilenamePath + '" .';
                const { stdoutY, stderrZ } = execSync(zipCommand);
                const response = JSON.parse(stdout);
                const accountID = response.Account;
                let cliCommand = 'aws lambda publish-layer-version --layer-name "' + cliLambdaLayerName + '" --description "' +
                    cliLambdaLayerVersionDescription + '" --compatible-runtimes ' + cliRuntime +
                    ' --zip-file fileb://' + cliLambdaLayerTempZipFilenamePath;
                cliCommand += ' --region ' + cliRegion;
                cliCommand += ' --profile ' + cliProfile;
                vscode_1.window.showInformationMessage(`Adding new lambda layer '${cliLambdaLayerName}' with '${cliCommand}'`);
                addLambdaLayerVersion(cliCommand, cliLambdaLayerName, cliLambdaLayerTempZipFilenamePath);
                return accountID;
            });
        }
        function addLambdaLayerVersion(cmd, layerName, tempZipFilename) {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec(cmd);
                console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                let resp = JSON.parse(stdout);
                const { stdoutE, stderrF } = execSync('del ' + tempZipFilename);
                let version = resp.Version;
                vscode_1.window.showInformationMessage(`Added new lambda layer '${layerName}:${version}'.`);
            });
        }
    });
}
exports.multiStepUpdateLambdaLayerVersion = multiStepUpdateLambdaLayerVersion;
function multiStepUpdateLambdaFunctionLayer(context) {
    return __awaiter(this, void 0, void 0, function* () {
    });
}
exports.multiStepUpdateLambdaFunctionLayer = multiStepUpdateLambdaFunctionLayer;
function multiStepDeployProductionLambdaFunction(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Deploy Production Lambda Function';
        const numSteps = 4;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        let functionName = '';
        let description = '';
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (profile.length > 0)
                    settingsReducedSteps++;
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputLambdaFunctionName(input, state));
                return state;
            });
        }
        function inputLambdaFunctionName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.functionName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.functionName === 'string' ? state.functionName : '',
                    prompt: 'Enter a lambda function name.',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                functionName = state.functionName;
                if (description.length > 0) {
                }
                else {
                    return (input) => inputLambdaLayerVersionDescription(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function inputLambdaLayerVersionDescription(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.description = yield input.showInputBox({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.description === 'string' ? state.description : '',
                    prompt: 'Enter a description for the new lambda function version.',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                description = state.description;
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
                if (profile.length > 0) {
                }
                else {
                    return (input) => inputProfile(input, state);
                }
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 3,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: state.profile || '',
                    prompt: 'Select an AWS profile (optional)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRuntimes(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['python3.6']
                    .map(label => ({ label }));
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        const util = require('util');
        const exec = util.promisify(require('child_process').exec);
        const execSync = require('child_process').execSync;
        let cliLambdaFunctionName = '';
        if (state.functionName.length > 0)
            cliLambdaFunctionName = state.functionName;
        let cliLambdaFunctionVersionDescription = '';
        cliLambdaFunctionVersionDescription = state.description;
        //cliFunctionAlias = state.functionAlias;
        let cliProfile = '';
        if (profile.length > 0)
            cliProfile = profile;
        else
            cliProfile = state.profile;
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec('aws sts get-caller-identity --profile ' + profile);
                //console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                const response = JSON.parse(stdout);
                const accountID = response.Account;
                vscode_1.window.showInformationMessage(`Deploying new '${cliLambdaFunctionName}' version with description '${cliLambdaFunctionVersionDescription}'`);
                //deploy version
                let versionCommand = 'aws lambda publish-version --function-name "' + cliLambdaFunctionName + '" --description "' + cliLambdaFunctionVersionDescription + '"';
                versionCommand += ' --region ' + cliRegion;
                versionCommand += ' --profile ' + cliProfile;
                addLambdaFunctionVersion(versionCommand, cliLambdaFunctionName);
                return accountID;
            });
        }
        function addLambdaFunctionVersion(cmd, functionName) {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec(cmd);
                console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                let resp = JSON.parse(stdout);
                let version = resp.Version;
                vscode_1.window.showInformationMessage('Added new lambda function version ' + version + ' for ' + functionName);
                let cliCommand = 'aws lambda update-alias --function-name "' + functionName + '" --name "Production" --function-version ' + version;
                cliCommand += ' --region ' + cliRegion;
                cliCommand += ' --profile ' + cliProfile;
                updateProductionLambdaVersion(cliCommand, functionName, version);
            });
        }
        function updateProductionLambdaVersion(cmd, functionName, versionNumber) {
            return __awaiter(this, void 0, void 0, function* () {
                const { stdout, stderr } = yield exec(cmd);
                console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                let resp = JSON.parse(stdout);
                vscode_1.window.showInformationMessage('Deployed new lambda function "' + functionName + '" with version ' + versionNumber + ' to Production.');
            });
        }
    });
}
exports.multiStepDeployProductionLambdaFunction = multiStepDeployProductionLambdaFunction;
function multiStepOpenLambdaFunctionAWSConsole(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Deploy Production Lambda Function';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        let functionName = '';
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputLambdaFunctionName(input, state));
                return state;
            });
        }
        function inputLambdaFunctionName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.functionName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.functionName === 'string' ? state.functionName : '',
                    prompt: 'Enter a lambda function name.',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                functionName = state.functionName;
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                //const { stdout, stderr } = await exec('aws sts get-caller-identity --profile '+profile);
                //console.log('stdout:', stdout);
                //console.log('stderr:', stderr);
                //const response = JSON.parse(stdout);
                //const accountID = response.Account;
                let url = 'https://' + region + '.console.aws.amazon.com/lambda/home?region=' + region + '#/functions/' + functionName + '/versions/$LATEST?tab=configuration';
                vscode.env.openExternal(vscode.Uri.parse(url));
                //return accountID;
                return null;
            });
        }
    });
}
exports.multiStepOpenLambdaFunctionAWSConsole = multiStepOpenLambdaFunctionAWSConsole;
function multiStepUpdateSettingsVPC(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS VPC Settings';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const vpc = vscode.workspace.getConfiguration().get('conf.view.awsVPC');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputVPC(input, state));
                return state;
            });
        }
        function inputVPC(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.vpcId = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.vpcId === 'string' ? state.vpcId : vpc,
                    prompt: 'Enter VPC Identifier',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsVPC', state.vpcId, true);
                const vpc = vscode.workspace.getConfiguration().get('conf.view.awsVPC');
                updateStatusBarVPCItem();
                vscode_1.window.showInformationMessage('VPC Identifier Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsVPC = multiStepUpdateSettingsVPC;
function multiStepUpdateSettingsProfile(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS Profile';
        const numSteps = 1;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                yield MultiStepInput.run(input => inputProfile(input, state));
                return state;
            });
        }
        function inputProfile(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.profile = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.profile === 'string' ? state.profile : profile,
                    prompt: 'Enter AWS Profile',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        const state = yield collectInputs();
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsProfile', state.profile, true);
                const vpc = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
                updateStatusBarProfileItem();
                vscode_1.window.showInformationMessage('AWS Profile Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsProfile = multiStepUpdateSettingsProfile;
function multiStepUpdateSettingsSubnet(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS VPC Subnet Settings';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputSubnet(input, state));
                return state;
            });
        }
        function inputSubnet(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.subnet = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.subnet === 'string' ? state.subnet : subnet,
                    prompt: 'Enter comma separated subnet list',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsVPCSubnet', state.subnet, true);
                const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet');
                updateStatusBarVPCSubnetItem();
                vscode_1.window.showInformationMessage('VPC Subnet Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsSubnet = multiStepUpdateSettingsSubnet;
function multiStepUpdateSettingsSecurityGroup(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS VPC Security Group Settings';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputSecurityGroup(input, state));
                return state;
            });
        }
        function inputSecurityGroup(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.securityGroup = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.securityGroup === 'string' ? state.securityGroup : securityGroup,
                    prompt: 'Enter comma separated security group list',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsVPCSecurityGroup', state.securityGroup, true);
                const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup');
                updateStatusBarVPCSecurityGroupItem();
                vscode_1.window.showInformationMessage('VPC Security Group Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsSecurityGroup = multiStepUpdateSettingsSecurityGroup;
function multiStepUpdateSettingsLambdaLayer(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS Lambda Layer';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const layerName = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayer');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputLayerName(input, state));
                return state;
            });
        }
        function inputLayerName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.layerName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.layerName === 'string' ? state.layerName : layerName,
                    prompt: 'Enter lambda layer name',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsLambdaLayer', state.layerName, true);
                const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayer');
                updateStatusBarLayerItem();
                vscode_1.window.showInformationMessage('Lambda Layer Name Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsLambdaLayer = multiStepUpdateSettingsLambdaLayer;
function multiStepUpdateSettingsLambdaLayers(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS Lambda Layer';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const layersName = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputLayerName(input, state));
                return state;
            });
        }
        function inputLayerName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.layersName = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.layersName === 'string' ? state.layersName : layersName,
                    prompt: 'Enter comma separated lambda layer arn list',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsLambdaLayers', state.layersName, true);
                const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers');
                updateStatusBarLayersItem();
                vscode_1.window.showInformationMessage('Lambda Layers ARN Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsLambdaLayers = multiStepUpdateSettingsLambdaLayers;
function multiStepUpdateSettingsLambdaMemory(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS Lambda Memory Settings';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputLayerName(input, state));
                return state;
            });
        }
        function inputLayerName(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.memory = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.memory === 'string' ? state.memory : memory,
                    prompt: 'Enter memory amount (MB)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsLambdaMemory', state.memory, true);
                const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory');
                updateStatusBarMemoryItem();
                vscode_1.window.showInformationMessage('Lambda Memory Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsLambdaMemory = multiStepUpdateSettingsLambdaMemory;
function multiStepUpdateSettingsLambdaTimeout(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS Lambda Timeout Settings';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputTimeout(input, state));
                return state;
            });
        }
        function inputTimeout(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.timeout = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.timeout === 'string' ? state.timeout : timeout,
                    prompt: 'Enter timeout amount (s)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsLambdaTimeout', state.timeout, true);
                const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout');
                updateStatusBarTimeoutItem();
                vscode_1.window.showInformationMessage('Lambda Timeout Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsLambdaTimeout = multiStepUpdateSettingsLambdaTimeout;
function multiStepUpdateSettingsExecutionRole(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS Lambda Execution Role Settings';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const role = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputTimeout(input, state));
                return state;
            });
        }
        function inputTimeout(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.role = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.role === 'string' ? state.role : role,
                    prompt: 'Enter timeout amount (s)',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsLambdaExecutionRole', state.role, true);
                const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole');
                updateStatusBarRoleItem();
                vscode_1.window.showInformationMessage('Lambda Execution Role Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsExecutionRole = multiStepUpdateSettingsExecutionRole;
function multiStepUpdateSettingsAPIGateway(context) {
    return __awaiter(this, void 0, void 0, function* () {
        let settingsReducedSteps = 0;
        const title = 'Update AWS API Gateway Settings';
        const numSteps = 2;
        const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile');
        const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion');
        const gateway = vscode.workspace.getConfiguration().get('conf.view.awsAPIGatewayID');
        function collectInputs() {
            return __awaiter(this, void 0, void 0, function* () {
                const state = {};
                if (region.length > 0)
                    settingsReducedSteps++;
                yield MultiStepInput.run(input => inputTimeout(input, state));
                return state;
            });
        }
        function inputTimeout(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                state.gateway = yield input.showInputBox({
                    title,
                    step: 1,
                    totalSteps: numSteps - settingsReducedSteps,
                    value: typeof state.gateway === 'string' ? state.gateway : gateway,
                    prompt: 'Enter Gateway ID',
                    validate: validateNameIsUnique,
                    shouldResume: shouldResume
                });
                if (region.length > 0) {
                }
                else {
                    return (input) => pickRegion(input, state);
                }
                //return (input: MultiStepInput) => inputAlias(input, state);
            });
        }
        function pickRegion(input, state) {
            return __awaiter(this, void 0, void 0, function* () {
                const regions = yield getAvailableRegions(state.region, undefined);
                state.region = yield input.showQuickPick({
                    title,
                    step: 2,
                    totalSteps: numSteps - settingsReducedSteps,
                    placeholder: 'Pick a region for your function',
                    items: regions,
                    activeItem: state.region,
                    shouldResume: shouldResume
                });
            });
        }
        function shouldResume() {
            // Could show a notification with the option to resume.
            return new Promise((resolve, reject) => {
            });
        }
        function validateNameIsUnique(name) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...validate...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return name === 'vscode' ? 'Name not unique' : undefined;
            });
        }
        function getAvailableRegions(resourceGroup, token) {
            return __awaiter(this, void 0, void 0, function* () {
                // ...retrieve...
                //await new Promise(resolve => setTimeout(resolve, 1000));
                return ['ap-southeast-1']
                    .map(label => ({ label }));
            });
        }
        const state = yield collectInputs();
        let cliRegion = '';
        if (region.length > 0)
            cliRegion = region;
        else
            cliRegion = state.region.label;
        startBuild();
        function startBuild() {
            return __awaiter(this, void 0, void 0, function* () {
                yield vscode.workspace.getConfiguration().update('conf.view.awsAPIGatewayID', state.gateway, true);
                const subnet = vscode.workspace.getConfiguration().get('conf.view.awsAPIGatewayID');
                updateStatusBarAPIGatewayItem();
                vscode_1.window.showInformationMessage('API Gateway ID Updated.');
                return null;
            });
        }
    });
}
exports.multiStepUpdateSettingsAPIGateway = multiStepUpdateSettingsAPIGateway;
// -------------------------------------------------------
// Helper code that wraps the API for the multi-step case.
// -------------------------------------------------------
class InputFlowAction {
    constructor() {
    }
}
InputFlowAction.back = new InputFlowAction();
InputFlowAction.cancel = new InputFlowAction();
InputFlowAction.resume = new InputFlowAction();
class MultiStepInput {
    constructor() {
        this.steps = [];
    }
    static run(start) {
        return __awaiter(this, void 0, void 0, function* () {
            const input = new MultiStepInput();
            return input.stepThrough(start);
        });
    }
    stepThrough(start) {
        return __awaiter(this, void 0, void 0, function* () {
            let step = start;
            while (step) {
                this.steps.push(step);
                if (this.current) {
                    this.current.enabled = false;
                    this.current.busy = true;
                }
                try {
                    step = yield step(this);
                }
                catch (err) {
                    if (err === InputFlowAction.back) {
                        this.steps.pop();
                        step = this.steps.pop();
                    }
                    else if (err === InputFlowAction.resume) {
                        step = this.steps.pop();
                    }
                    else if (err === InputFlowAction.cancel) {
                        step = undefined;
                    }
                    else {
                        throw err;
                    }
                }
            }
            if (this.current) {
                this.current.dispose();
            }
        });
    }
    showQuickPick({ title, step, totalSteps, items, activeItem, placeholder, buttons, shouldResume }) {
        return __awaiter(this, void 0, void 0, function* () {
            const disposables = [];
            try {
                return yield new Promise((resolve, reject) => {
                    const input = vscode_1.window.createQuickPick();
                    input.title = title;
                    input.step = step;
                    input.totalSteps = totalSteps;
                    input.placeholder = placeholder;
                    input.items = items;
                    if (activeItem) {
                        input.activeItems = [activeItem];
                    }
                    input.buttons = [
                        ...(this.steps.length > 1 ? [vscode_1.QuickInputButtons.Back] : []),
                        ...(buttons || [])
                    ];
                    disposables.push(input.onDidTriggerButton(item => {
                        if (item === vscode_1.QuickInputButtons.Back) {
                            reject(InputFlowAction.back);
                        }
                        else {
                            resolve(item);
                        }
                    }), input.onDidChangeSelection(items => resolve(items[0])), input.onDidHide(() => {
                        (() => __awaiter(this, void 0, void 0, function* () {
                            reject(shouldResume && (yield shouldResume()) ? InputFlowAction.resume : InputFlowAction.cancel);
                        }))()
                            .catch(reject);
                    }));
                    if (this.current) {
                        this.current.dispose();
                    }
                    this.current = input;
                    this.current.show();
                });
            }
            finally {
                disposables.forEach(d => d.dispose());
            }
        });
    }
    showInputBox({ title, step, totalSteps, value, prompt, validate, buttons, shouldResume }) {
        return __awaiter(this, void 0, void 0, function* () {
            const disposables = [];
            try {
                return yield new Promise((resolve, reject) => {
                    const input = vscode_1.window.createInputBox();
                    input.title = title;
                    input.step = step;
                    input.totalSteps = totalSteps;
                    input.value = value || '';
                    input.prompt = prompt;
                    input.buttons = [
                        ...(this.steps.length > 1 ? [vscode_1.QuickInputButtons.Back] : []),
                        ...(buttons || [])
                    ];
                    let validating = validate('');
                    disposables.push(input.onDidTriggerButton(item => {
                        if (item === vscode_1.QuickInputButtons.Back) {
                            reject(InputFlowAction.back);
                        }
                        else {
                            resolve(item);
                        }
                    }), input.onDidAccept(() => __awaiter(this, void 0, void 0, function* () {
                        const value = input.value;
                        input.enabled = false;
                        input.busy = true;
                        if (!(yield validate(value))) {
                            resolve(value);
                        }
                        input.enabled = true;
                        input.busy = false;
                    })), input.onDidChangeValue((text) => __awaiter(this, void 0, void 0, function* () {
                        const current = validate(text);
                        validating = current;
                        const validationMessage = yield current;
                        if (current === validating) {
                            input.validationMessage = validationMessage;
                        }
                    })), input.onDidHide(() => {
                        (() => __awaiter(this, void 0, void 0, function* () {
                            reject(shouldResume && (yield shouldResume()) ? InputFlowAction.resume : InputFlowAction.cancel);
                        }))()
                            .catch(reject);
                    }));
                    if (this.current) {
                        this.current.dispose();
                    }
                    this.current = input;
                    this.current.show();
                });
            }
            finally {
                disposables.forEach(d => d.dispose());
            }
        });
    }
}
//# sourceMappingURL=multiStepInput.js.map