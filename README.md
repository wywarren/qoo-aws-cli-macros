# qoo-aws-cli-macros README

This is a collection of AWS CLI commands simplified with the help of VS Code UI via extensions.

## Features

Create Lambda functions with boilerplate code and Lambda layers pre-attached with a few simple inputs.

## Requirements

Preconfigured AWS profile. If not already installed, install AWS CLI and configure your profile using `aws configure --profile myprofile`

## Extension Settings

Inside the extension settings you can set your predefined reusable profile settings for faster creation of CLI commands.

## Known Issues

Order step # may be off if not all settings fields are provided.

## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release of `qoo-aws-cli-macros`

### 1.0.1

Added API Gateway function permissions add and remove.

### 1.0.2

Added Lambda layer version upload support.

### 1.0.3

Added Deploy Production Lambda Function.

### 1.0.4

Open Lambda function in AWS console.

### 1.0.5

Create functions with all parameters. Status bar settings modification support.

### 1.0.6

Update CRUD functions with all parameters using base name.
### For more information

* [Qoobit Productions Inc.](https://qoobit.com)

**Enjoy!**
