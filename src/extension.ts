// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { window, commands, ExtensionContext } from 'vscode';
import { showQuickPick, showInputBox } from './basicInput';
import { multiStepCreateLambdaFunction } from './multiStepInput';
import { multiStepUpdateLambdaFunction } from './multiStepInput';
import { multiStepCreateLambdaFunctionCRUD } from './multiStepInput';
import { multiStepUpdateLambdaFunctionCRUD } from './multiStepInput';
import { multiStepAddLambdaFunctionPermissions } from './multiStepInput';
import { multiStepRemoveLambdaFunctionPermissions } from './multiStepInput';
import { multiStepUpdateLambdaLayerVersion } from './multiStepInput';
import { multiStepUpdateLambdaFunctionLayer } from './multiStepInput';
import { multiStepDeployProductionLambdaFunction } from './multiStepInput';
import { multiStepOpenLambdaFunctionAWSConsole } from './multiStepInput';

import { multiStepUpdateSettingsVPC } from './multiStepInput';
import { multiStepUpdateSettingsProfile } from './multiStepInput';
import { multiStepUpdateSettingsSubnet } from './multiStepInput';
import { multiStepUpdateSettingsSecurityGroup } from './multiStepInput';
import { multiStepUpdateSettingsLambdaLayer } from './multiStepInput';
import { multiStepUpdateSettingsLambdaLayers } from './multiStepInput';
import { multiStepUpdateSettingsLambdaMemory } from './multiStepInput';
import { multiStepUpdateSettingsLambdaTimeout } from './multiStepInput';
import { multiStepUpdateSettingsExecutionRole } from './multiStepInput';
import { multiStepUpdateSettingsAPIGateway } from './multiStepInput';



import { vpcStatusBarItem, updateStatusBarVPCItem } from './multiStepInput';
import { profileStatusBarItem, updateStatusBarProfileItem } from './multiStepInput';
import { apiGatewayStatusBarItem, updateStatusBarAPIGatewayItem } from './multiStepInput';
import { roleStatusBarItem, updateStatusBarRoleItem } from './multiStepInput';
import { subnetStatusBarItem, updateStatusBarVPCSubnetItem } from './multiStepInput';
import { securityGroupStatusBarItem, updateStatusBarVPCSecurityGroupItem } from './multiStepInput';
import { layerStatusBarItem, updateStatusBarLayerItem } from './multiStepInput';
import { layersStatusBarItem, updateStatusBarLayersItem } from './multiStepInput';
import { memoryStatusBarItem, updateStatusBarMemoryItem } from './multiStepInput';
import { timeoutStatusBarItem, updateStatusBarTimeoutItem } from './multiStepInput';


import { quickOpen } from './quickOpen';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: ExtensionContext) {
    
	const profileCommandId = 'extension.updateAWSProfile';
	context.subscriptions.push(vscode.commands.registerCommand(profileCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsProfile(context);
	}));
	profileStatusBarItem.command = profileCommandId;
	context.subscriptions.push(profileStatusBarItem);
    updateStatusBarProfileItem();
    

    const apiGatewayCommandId = 'extension.updateAPIGateway';
	context.subscriptions.push(vscode.commands.registerCommand(apiGatewayCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsAPIGateway(context);
    }));
	apiGatewayStatusBarItem.command = apiGatewayCommandId;
	context.subscriptions.push(apiGatewayStatusBarItem);
	updateStatusBarAPIGatewayItem();
    
    const roleCommandId = 'extension.updateExecutionRole';
	context.subscriptions.push(vscode.commands.registerCommand(roleCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsExecutionRole(context);
	}));
	roleStatusBarItem.command = roleCommandId;
	context.subscriptions.push(roleStatusBarItem);
    updateStatusBarRoleItem();
    
    const vpcCommandId = 'extension.updateVPC';
	context.subscriptions.push(vscode.commands.registerCommand(vpcCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsVPC(context);
	}));
	vpcStatusBarItem.command = vpcCommandId;
	context.subscriptions.push(vpcStatusBarItem);
    updateStatusBarVPCItem();

    const subnetCommandId = 'extension.updateVPCSubnet';
	context.subscriptions.push(vscode.commands.registerCommand(subnetCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsSubnet(context);
	}));
	subnetStatusBarItem.command = subnetCommandId;
	context.subscriptions.push(subnetStatusBarItem);
    updateStatusBarVPCSubnetItem();
    
    const securityGroupCommandId = 'extension.updateSecurityGroup';
	context.subscriptions.push(vscode.commands.registerCommand(securityGroupCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsSecurityGroup(context);
	}));
	securityGroupStatusBarItem.command = securityGroupCommandId;
	context.subscriptions.push(securityGroupStatusBarItem);
    updateStatusBarVPCSecurityGroupItem();
    
    const layerCommandId = 'extension.updateLambdaLayer';
	context.subscriptions.push(vscode.commands.registerCommand(layerCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsLambdaLayer(context);
	}));
	layerStatusBarItem.command = layerCommandId;
	context.subscriptions.push(layerStatusBarItem);
    updateStatusBarLayerItem();

    const layersCommandId = 'extension.updateLambdaLayers';
	context.subscriptions.push(vscode.commands.registerCommand(layersCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsLambdaLayers(context);
	}));
	layersStatusBarItem.command = layersCommandId;
	context.subscriptions.push(layersStatusBarItem);
    updateStatusBarLayersItem();

    const memoryCommandId = 'extension.updateLambdaMemory';
	context.subscriptions.push(vscode.commands.registerCommand(memoryCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsLambdaMemory(context);
	}));
	memoryStatusBarItem.command = memoryCommandId;
	context.subscriptions.push(memoryStatusBarItem);
    updateStatusBarMemoryItem();

    const timeoutCommandId = 'extension.updateLambdaTimeout';
	context.subscriptions.push(vscode.commands.registerCommand(timeoutCommandId, () => {
        const lambdaSteps = multiStepUpdateSettingsLambdaTimeout(context);
	}));
	timeoutStatusBarItem.command = timeoutCommandId;
	context.subscriptions.push(timeoutStatusBarItem);
    updateStatusBarTimeoutItem();

    /*
    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "qoo-aws-cli-macro" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('extension.createAWSLambdaFunction', () => {
        // The code you place here will be executed every time your command is executed

        let functionName = vscode.window.showInputBox();
        console.log(functionName);
        // Display a message box to the user
        vscode.window.showInformationMessage('Lambda function has been created.');
    });

    context.subscriptions.push(disposable);
    */
    
    context.subscriptions.push(commands.registerCommand('extension.createAWSLambdaFunction', async () => {
        /*
        const options: { [key: string]: (context: ExtensionContext) => Promise<void> } = {
            showQuickPick,
            showInputBox,
            multiStepInput,
            quickOpen,
        };
        const quickPick = window.createQuickPick();
        quickPick.items = Object.keys(options).map(label => ({ label }));
        quickPick.onDidChangeSelection(selection => {
            if (selection[0]) {
                options[selection[0].label](context)
                    .catch(console.error);
            }
        });
        quickPick.onDidHide(() => quickPick.dispose());
        quickPick.show();
        */
        const lambdaSteps = multiStepCreateLambdaFunction(context);
        
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateAWSLambdaFunction', async () => {
        const lambdaSteps = multiStepUpdateLambdaFunction(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.createAWSLambdaFunctionCRUD', async () => {
        const lambdaSteps = multiStepCreateLambdaFunctionCRUD(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateAWSLambdaFunctionCRUD', async () => {
        const lambdaSteps = multiStepUpdateLambdaFunctionCRUD(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.addAWSLambdaFunctionAPIGatewayPermission', async () => {
        const lambdaSteps = multiStepAddLambdaFunctionPermissions(context);
    }));
    context.subscriptions.push(commands.registerCommand('extension.removeAWSLambdaFunctionAPIGatewayPermission', async () => {
        const lambdaSteps = multiStepRemoveLambdaFunctionPermissions(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateLambdaLayerVersion', async () => {
        const lambdaSteps = multiStepUpdateLambdaLayerVersion(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateLambdaFunctionLayer', async () => {
        const lambdaSteps = multiStepUpdateLambdaFunctionLayer(context);
    }));
    
    context.subscriptions.push(commands.registerCommand('extension.deployProductionLambdaFunction', async () => {
        const lambdaSteps = multiStepDeployProductionLambdaFunction(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.openLambdaFunctionAWSConsole', async () => {
        const lambdaSteps = multiStepOpenLambdaFunctionAWSConsole(context);
    }));


    //settings
    context.subscriptions.push(commands.registerCommand('extension.updateSettingsVPC', async () => {
        const lambdaSteps = multiStepUpdateSettingsVPC(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsProfile', async () => {
        const lambdaSteps = multiStepUpdateSettingsVPC(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsSubnet', async () => {
        const lambdaSteps = multiStepUpdateSettingsSubnet(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsSecurityGroup', async () => {
        const lambdaSteps = multiStepUpdateSettingsSecurityGroup(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsLambdaLayer', async () => {
        const lambdaSteps = multiStepUpdateSettingsLambdaLayer(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsLambdaLayers', async () => {
        const lambdaSteps = multiStepUpdateSettingsLambdaLayers(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsLambdaMemory', async () => {
        const lambdaSteps = multiStepUpdateSettingsLambdaMemory(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsLambdaTimeout', async () => {
        const lambdaSteps = multiStepUpdateSettingsLambdaTimeout(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsExecutionRole', async () => {
        const lambdaSteps = multiStepUpdateSettingsExecutionRole(context);
    }));

    context.subscriptions.push(commands.registerCommand('extension.updateSettingsAPIGateway', async () => {
        const lambdaSteps = multiStepUpdateSettingsAPIGateway(context);
    }));
}

// this method is called when your extension is deactivated
export function deactivate() {}

