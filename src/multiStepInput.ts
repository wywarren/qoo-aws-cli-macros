/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

import * as vscode from 'vscode';
import { QuickPickItem, window, Disposable, CancellationToken, QuickInputButton, QuickInput, ExtensionContext, QuickInputButtons, Uri } from 'vscode';
import { stat } from 'fs';

export const vpcStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const profileStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const apiGatewayStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const roleStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const subnetStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const securityGroupStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const layerStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const layersStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const memoryStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
export const timeoutStatusBarItem: vscode.StatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);

export async function updateStatusBarVPCItem() {
    const vpc = vscode.workspace.getConfiguration().get('conf.view.awsVPC') as string;
    vpcStatusBarItem.text = "VPC: " + vpc;
    vpcStatusBarItem.show();
}

export async function updateStatusBarProfileItem() {
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    profileStatusBarItem.text = "Profile: " + profile;
    profileStatusBarItem.show();
}

export async function updateStatusBarVPCSubnetItem() {
    const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet') as string;
    subnetStatusBarItem.text = "Subnet: " + subnet;
    subnetStatusBarItem.show();
}

export async function updateStatusBarVPCSecurityGroupItem() {
    const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup') as string;
    securityGroupStatusBarItem.text = "Security Group: " + securityGroup;
    securityGroupStatusBarItem.show();
}

export async function updateStatusBarLayerItem() {
    const layer = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayer') as string;
    layerStatusBarItem.text = "Layer: " + layer;
    layerStatusBarItem.show();
}

export async function updateStatusBarLayersItem() {
    const layer = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers') as string;
    layersStatusBarItem.text = "Layers: " + layer;
    layersStatusBarItem.show();
}

export async function updateStatusBarAPIGatewayItem() {
    const apiGatewayID = vscode.workspace.getConfiguration().get('conf.view.awsAPIGatewayID') as string;
    apiGatewayStatusBarItem.text = "API: " + apiGatewayID;
    apiGatewayStatusBarItem.show();
}

export async function updateStatusBarRoleItem() {
    const role = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole') as string;
    roleStatusBarItem.text = "Role: " + role;
    roleStatusBarItem.show();
}

export async function updateStatusBarMemoryItem() {
    const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory') as string;
    memoryStatusBarItem.text = "Memory: " + memory + "MB";
    memoryStatusBarItem.show();
}

export async function updateStatusBarTimeoutItem() {
    const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout') as string;
    timeoutStatusBarItem.text = "Timeout: " + timeout + "s";
    timeoutStatusBarItem.show();
}

/**
 * A multi-step input using window.createQuickPick() and window.createInputBox().
 * 
 * This first part uses the helper class `MultiStepInput` that wraps the API for the multi-step case.
 */

export async function multiStepAddLambdaFunctionPermissions(context: ExtensionContext) {

    let settingsReducedSteps = 0;
	const title = 'Add AWS Lambda Function Permissions for API Gateway';
    const numSteps = 6;
    
    const apiGatewayID = vscode.workspace.getConfiguration().get('conf.view.awsAPIGatewayID') as string;
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const statementID = vscode.workspace.getConfiguration().get('conf.view.awsStatementID') as string;
    
    class MyButton implements QuickInputButton {
        constructor(public iconPath: { light: Uri; dark: Uri; }, public tooltip: string) { }
    }

    interface State {
		title: string;
		step: number;
		totalSteps: number;       
        functionName: string;
        functionAlias: string;
        resourcePath: string;
        statementID: string;
        apiGatewayID: string;
        region: QuickPickItem;
		profile: string;
    }
    
    async function collectInputs() {
        

        const state = {} as Partial<State>;
        
        if(profile.length>0){
            settingsReducedSteps++;
        }
        if(region.length>0){
            settingsReducedSteps++;
        }
        if(apiGatewayID.length>0){
            settingsReducedSteps++;
        }
        if(statementID.length>0){
            settingsReducedSteps++;
        }
        
        await MultiStepInput.run(input => inputLambdaFunctionName(input, state));
        return state as State;
    }

    
    async function inputLambdaFunctionName(input: MultiStepInput, state: Partial<State>) {
        state.functionName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.functionName === 'string' ? state.functionName : '',
            prompt: 'Enter the name of an existing Lambda function',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });

        return (input: MultiStepInput) => inputResourcePath(input, state);
    }

    /*
    async function inputAlias(input: MultiStepInput, state: Partial<State>) {
        
        state.functionAlias = await input.showInputBox({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.functionAlias || '',
            prompt: 'Enter the name of function alias to grant permission to',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        return (input: MultiStepInput) => inputResourcePath(input, state);
    }
    */
    
    async function inputResourcePath(input: MultiStepInput, state: Partial<State>) {
        
        state.resourcePath = await input.showInputBox({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.resourcePath || '',
            prompt: 'Enter the name of the API Gateway resource path to grant permission to',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(apiGatewayID.length>0){
            if(statementID.length>0){
            
                if(region.length>0){
                    if(profile.length>0){
                    
                    }
                    else{
                        return (input: MultiStepInput) => inputProfile(input, state);
                    }
                }
                else{
                    return (input: MultiStepInput) => pickRegion(input, state);
                }
            
            }
            else{
                return (input: MultiStepInput) => inputStatementID(input, state);
            }
            
        }
        else{
            return (input: MultiStepInput) => inputApiGatewayID(input, state);
        }
        
	}
	
	async function inputApiGatewayID(input: MultiStepInput, state: Partial<State>) {
        
        let currentStep = 3;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 3;
        }
        state.apiGatewayID = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.apiGatewayID || '',
            prompt: 'Enter API Gateway ID',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(statementID.length>0){
            if(region.length>0){
                if(profile.length>0){
                
                }
                else{
                    return (input: MultiStepInput) => inputProfile(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => pickRegion(input, state);
            }
        
        }
        else{
            return (input: MultiStepInput) => inputStatementID(input, state);
        }
        
    }
    
    async function inputStatementID(input: MultiStepInput, state: Partial<State>) {
        let currentStep = 4;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 3;
        }
        state.statementID = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.statementID || '',
            prompt: 'Enter a Statement ID',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
    
        if(region.length>0){
            if(profile.length>0){
            
            }
            else{
                return (input: MultiStepInput) => inputProfile(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
    
        
    }

    /*
    async function inputAccountID(input: MultiStepInput, state: Partial<State>) {
        
        state.accountID = await input.showInputBox({
            title,
            step: 6,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.accountID || '',
            prompt: 'Enter an Account ID',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            if(profile.length>0){
            
            }
            else{
                return (input: MultiStepInput) => inputProfile(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
    }
    */

    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined /* TODO: token */);
        
        state.region = await input.showQuickPick({
            title,
            step: 5,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
        if(profile.length>0){
            
        }
        else{
            return (input: MultiStepInput) => inputProfile(input, state);
        }
		
		
	}
	
	async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        
        state.profile = await input.showInputBox({
            title,
            step: 6,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.profile || '',
            prompt: 'Select an AWS profile (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
	}

    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	async function getAvailableRuntimes(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['python3.6']
			.map(label => ({ label }));
	}
	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    const util = require('util');
    const exec = util.promisify(require('child_process').exec);

    let cliFunctionName = '';
    cliFunctionName = state.functionName;
    
    //cliFunctionAlias = state.functionAlias;
    let cliAPIGatewayResourcePath = '';
    cliAPIGatewayResourcePath = state.resourcePath;

    let cliAccountID = '';
    
    let cliStatementID = '';
    if(statementID.length>0) cliStatementID = statementID;
    else cliStatementID = state.statementID;
    
    let cliAPIGatewayID = '';
    if(apiGatewayID.length>0) cliAPIGatewayID = apiGatewayID;
    else cliAPIGatewayID = state.apiGatewayID;

    let cliProfile = '';
    if(profile.length>0) cliProfile = profile;
    else cliProfile = state.profile;

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;

    startBuild();

    async function startBuild(){
        const { stdout, stderr } = await exec('aws sts get-caller-identity --profile '+cliProfile);
        //console.log('stdout:', stdout);
        //console.log('stderr:', stderr);

        const response = JSON.parse(stdout);
        const accountID = response.Account;
        
        cliAccountID = String(accountID);

        let cliStatementIDD = cliStatementID+'-1';
        let cliFunctionAlias = 'Production';

        let cliCommand = 'aws lambda add-permission  --function-name "arn:aws:lambda:'+
            cliRegion+':'+cliAccountID+':function:'+cliFunctionName+':'+cliFunctionAlias+'"'+
            ' --source-arn "arn:aws:execute-api:'+cliRegion+':'+cliAccountID+':'+cliAPIGatewayID+'/*/'+cliAPIGatewayResourcePath+
            '" --principal apigateway.amazonaws.com --statement-id '+cliStatementIDD+
            ' --action lambda:InvokeFunction';
        cliCommand += ' --region '+cliRegion;
        cliCommand += ' --profile '+cliProfile;

        window.showInformationMessage(`Adding Lambda Function '${cliFunctionName}:${cliFunctionAlias}' permissions with ${cliCommand}`);
        
        addLambdaFunctionPermission(cliCommand, cliFunctionName, cliFunctionAlias);

        cliStatementIDD = cliStatementID+'-2';
        cliFunctionAlias = 'Development';
        cliCommand = 'aws lambda add-permission  --function-name "arn:aws:lambda:'+
            cliRegion+':'+cliAccountID+':function:'+cliFunctionName+':'+cliFunctionAlias+'"'+
            ' --source-arn "arn:aws:execute-api:'+cliRegion+':'+cliAccountID+':'+cliAPIGatewayID+'/*/'+cliAPIGatewayResourcePath+
            '" --principal apigateway.amazonaws.com --statement-id '+cliStatementIDD+
            ' --action lambda:InvokeFunction';
        cliCommand += ' --region '+cliRegion;
        cliCommand += ' --profile '+cliProfile;

        window.showInformationMessage(`Adding Lambda Function '${cliFunctionName}:${cliFunctionAlias}' permissions with ${cliCommand}`);
        
        addLambdaFunctionPermission(cliCommand, cliFunctionName, cliFunctionAlias);
        
        
        return accountID;
    }

    async function addLambdaFunctionPermission(cmd:string, functionName:string, alias:string) {
        const { stdout, stderr } = await exec(cmd);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
        window.showInformationMessage(`Added Lambda Function '${functionName}:${alias}' permissions to API Gateway.`);
    }
}

export async function multiStepRemoveLambdaFunctionPermissions(context: ExtensionContext) {

    let settingsReducedSteps = 0;
	const title = 'Remove AWS Lambda Function Permissions for API Gateway';
    const numSteps = 3;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const statementID = vscode.workspace.getConfiguration().get('conf.view.awsStatementID') as string;
    
    interface State {
		title: string;
		step: number;
		totalSteps: number;       
        functionName: string;
        statementID: string;
        region: QuickPickItem;
		profile: string;
    }
    
    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(profile.length>0) settingsReducedSteps++;
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputLambdaFunctionName(input, state));
        return state as State;
    }

    
    async function inputLambdaFunctionName(input: MultiStepInput, state: Partial<State>) {
        state.functionName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.functionName === 'string' ? state.functionName : '',
            prompt: 'Enter the name of an existing Lambda function',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }

    /*
    async function inputAlias(input: MultiStepInput, state: Partial<State>) {
        
        state.functionAlias = await input.showInputBox({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.functionAlias || '',
            prompt: 'Enter the name of function alias to remove permission from',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(statementID.length>0){
            
            if(region.length>0){
                if(profile.length>0){
                
                }
                else{
                    return (input: MultiStepInput) => inputProfile(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => pickRegion(input, state);
            }
        
        }
        else{
            return (input: MultiStepInput) => inputStatementID(input, state);
        }
        
    }
    
    */
    /*
    async function inputStatementID(input: MultiStepInput, state: Partial<State>) {
        let currentStep = 3;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 3;
        }
        state.statementID = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.statementID || '',
            prompt: 'Enter a Statement ID',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
        if(region.length>0){
            if(profile.length>0){
            
            }
            else{
                return (input: MultiStepInput) => inputProfile(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
    
        
    }
    */
    
    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined /* TODO: token */);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
        if(profile.length>0){
            
        }
        else{
            return (input: MultiStepInput) => inputProfile(input, state);
        }
		
		
	}
	
	async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        
        state.profile = await input.showInputBox({
            title,
            step: 3,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.profile || '',
            prompt: 'Select an AWS profile (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
    }
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	async function getAvailableRuntimes(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['python3.6']
			.map(label => ({ label }));
	}
	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    const util = require('util');
    const exec = util.promisify(require('child_process').exec);

    let cliFunctionName = '';
    cliFunctionName = state.functionName;
    
    let cliAccountID = '';
    
    let cliProfile = '';
    if(profile.length>0) cliProfile = profile;
    else cliProfile = state.profile;

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;

    let cliStatementID = '';
    if(statementID.length>0) cliStatementID = statementID;
    else cliStatementID = state.statementID;
    
    startBuild();
    
    async function startBuild(){
        const { stdout, stderr } = await exec('aws sts get-caller-identity --profile ' + profile);
        //console.log('stdout:', stdout);
        //console.log('stderr:', stderr);

        const response = JSON.parse(stdout);
        const accountID = response.Account;

        cliAccountID = String(accountID);

        let cliFunctionAlias = 'Production';
        let cliStatementIDD = cliStatementID+'-1';
        
        let cliCommand = 'aws lambda remove-permission  --function-name "arn:aws:lambda:'+
            cliRegion+':'+cliAccountID+':function:'+cliFunctionName+':'+cliFunctionAlias+'"'+
            ' --statement-id '+cliStatementIDD;
        cliCommand += ' --region '+cliRegion;
        cliCommand += ' --profile '+cliProfile;


        window.showInformationMessage(`Removing Lambda Function '${cliFunctionName}:${cliFunctionAlias}' permissions with ${cliCommand}`);
        
        addLambdaFunctionPermission(cliCommand, cliFunctionName, cliFunctionAlias);
        

        cliFunctionAlias = 'Development';
        cliStatementIDD = cliStatementID+'-2';
        
        cliCommand = 'aws lambda remove-permission  --function-name "arn:aws:lambda:'+
            cliRegion+':'+cliAccountID+':function:'+cliFunctionName+':'+cliFunctionAlias+'"'+
            ' --statement-id '+cliStatementIDD;
        cliCommand += ' --region '+cliRegion;
        cliCommand += ' --profile '+cliProfile;


        window.showInformationMessage(`Removing Lambda Function '${cliFunctionName}:${cliFunctionAlias}' permissions with ${cliCommand}`);
        
        addLambdaFunctionPermission(cliCommand, cliFunctionName, cliFunctionAlias);
            
        return accountID;
    }

    async function addLambdaFunctionPermission(cmd:string, functionName:string, alias:string) {
        const { stdout, stderr } = await exec(cmd);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);

        window.showInformationMessage(`Removed Lambda Function '${functionName}:${alias}' permissions to API Gateway.`);
    }
    
}

export async function multiStepCreateLambdaFunction(context: ExtensionContext) {

    let settingsReducedSteps = 0;
	const title = 'Create AWS Lambda Function';
    const numSteps = 9;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const accountID = vscode.workspace.getConfiguration().get('conf.view.awsAccountID') as string;
    const executionRole = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole') as string;
    const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime') as string;
    const boilerplateS3Bucket = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Bucket') as string;
    const boilerplateS3Key = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Key') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;

    const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory') as string;
    const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout') as string;
    const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet') as string;
    const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup') as string;
    const layers = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers') as string;
    
    class MyButton implements QuickInputButton {
        constructor(public iconPath: { light: Uri; dark: Uri; }, public tooltip: string) { }
    }

    const createResourceGroupButton = new MyButton({
        dark: Uri.file(context.asAbsolutePath('resources/dark/add.svg')),
        light: Uri.file(context.asAbsolutePath('resources/light/add.svg')),
    }, 'Create Resource Group');

    const resourceGroups: QuickPickItem[] = ['vscode-data-function', 'vscode-appservice-microservices', 'vscode-appservice-monitor', 'vscode-appservice-preview', 'vscode-appservice-prod']
        .map(label => ({ label }));

    interface State {
		title: string;
		step: number;
		totalSteps: number;       
		functionName: string;
		runtime: QuickPickItem;
		role: string;
		layer: string;
		layerVersion: string;
		s3Bucket:string;
		s3Key:string;
		region: QuickPickItem;
		//accountID: string;
        profile: string;
        layers:string;
    }
    
    async function collectInputs() {
        

        const state = {} as Partial<State>;
        
        if(profile.length>0){
            settingsReducedSteps++;
        }
        if(executionRole.length>0){
            settingsReducedSteps++;
        }
        if(runtime.length>0){
            settingsReducedSteps++;
        }
        if(boilerplateS3Bucket.length>0){
            settingsReducedSteps++;
        }
        if(boilerplateS3Key.length>0){
            settingsReducedSteps++;
        }
        if(region.length>0){
            settingsReducedSteps++;
        }
        if(layers.length>0){
            settingsReducedSteps++;
            settingsReducedSteps++;
        }
        
        /*
        await MultiStepInput.run(input => pickResourceGroup(input, state));
        */
        await MultiStepInput.run(input => inputLambdaFunctionName(input, state));
        return state as State;
    }

    
    /*
    async function pickResourceGroup(input: MultiStepInput, state: Partial<State>) {
        const pick = await input.showQuickPick({
            title,
            step: 1,
            totalSteps: 3,
            placeholder: 'Pick a resource group',
            items: resourceGroups,
            activeItem: typeof state.resourceGroup !== 'string' ? state.resourceGroup : undefined,
            buttons: [createResourceGroupButton],
            shouldResume: shouldResume
        });
        if (pick instanceof MyButton) {
            return (input: MultiStepInput) => inputResourceGroupName(input, state);
        }
        state.resourceGroup = pick;
        return (input: MultiStepInput) => inputName(input, state);
    }
    */
    async function inputLambdaFunctionName(input: MultiStepInput, state: Partial<State>) {
        state.functionName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.functionName === 'string' ? state.functionName : '',
            prompt: 'Choose a unique name for the lambda function',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });

        if(runtime.length>0){
            if(executionRole.length>0){
                if(region.length>0){
                    if(profile.length>0){
                        if(layers.length>0){

                        }
                        else{
                            return (input: MultiStepInput) => inputLayer(input, state);
                        }
                        
                    }
                    else{
                        return (input: MultiStepInput) => inputProfile(input, state);
                    }
                }
                else{
                    return (input: MultiStepInput) => pickRegion(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => inputRole(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRuntime(input, state);
        }
            
    }

    async function pickRuntime(input: MultiStepInput, state: Partial<State>) {
        /*
        const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
        */
        const runtimes = await getAvailableRuntimes(state.runtime!, undefined /* TODO: token */);
        
        state.runtime = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a runtime',
            items: runtimes,
            activeItem: state.runtime,
            shouldResume: shouldResume
        });

        if(executionRole.length>0){
            if(region.length>0){
                if(profile.length>0){
                    return (input: MultiStepInput) => inputLayer(input, state);
                }
                else{
                    return (input: MultiStepInput) => inputProfile(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => pickRegion(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => inputRole(input, state);
        }
        
    }

    async function inputRole(input: MultiStepInput, state: Partial<State>) {
        const additionalSteps = typeof state.functionName === 'string' ? 1 : 0;
        // TODO: Remember current value when navigating back.
        state.role = await input.showInputBox({
            title,
            step: 3,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.role || '',
            prompt: 'Select an execution role',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            if(profile.length>0){
                return (input: MultiStepInput) => inputLayer(input, state);
            }
            else{
                return (input: MultiStepInput) => inputProfile(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        
	}
	
	async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        /*
        const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
        */
        const regions = await getAvailableRegions(state.region!, undefined /* TODO: token */);
        // TODO: Remember currently active item when navigating back.
        state.region = await input.showQuickPick({
            title,
            step: 4,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
        if(profile.length>0){
            return (input: MultiStepInput) => inputLayer(input, state);
        }
        else{
            return (input: MultiStepInput) => inputProfile(input, state);
        }
		
		
	}
	
	async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        
        state.profile = await input.showInputBox({
            title,
            step: 5,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.profile || '',
            prompt: 'Select an AWS profile (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
        return (input: MultiStepInput) => inputLayer(input, state);
        
	}

    //merge to this layer
	async function inputLayer(input: MultiStepInput, state: Partial<State>) {
        
        let currentStep = 6;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 2;
        }
        state.layer = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.layer || '',
            prompt: 'Pick a layer to add to your function (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        return (input: MultiStepInput) => inputLayerVersion(input, state);
        
	}

	async function inputLayerVersion(input: MultiStepInput, state: Partial<State>) {
        let currentStep = 7;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 2;
        }
        state.layerVersion = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.layerVersion || '',
            prompt: 'Pick a layer version',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(boilerplateS3Bucket.length>0){
            if(boilerplateS3Key.length>0){

            }
            else{
                return (input: MultiStepInput) => inputS3Key(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => inputS3Bucket(input, state);
        }
        
	}

	async function inputS3Bucket(input: MultiStepInput, state: Partial<State>) {
        
        state.s3Bucket = await input.showInputBox({
            title,
            step: 8,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.s3Bucket || '',
            prompt: 'Pick a S3 Bucket',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(boilerplateS3Key.length>0){

        }
        else{
            return (input: MultiStepInput) => inputS3Key(input, state);
        }
        
        
	}

	async function inputS3Key(input: MultiStepInput, state: Partial<State>) {
        
        state.s3Key = await input.showInputBox({
            title,
            step: 9,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.s3Key || '',
            prompt: 'Enter a S3 Key',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        /*
        return (input: MultiStepInput) => pickRegion(input, state);
        */
    }
    

    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	async function getAvailableRuntimes(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['python3.6']
			.map(label => ({ label }));
	}
	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    const util = require('util');
    //const execSync = util.promisify(require('child_process').execSync);
    const execSync = require('child_process').execSync;
    const exec = util.promisify(require('child_process').exec);

    let cliFunctionName = '';
    cliFunctionName = state.functionName;
    let cliAccountID = '';
    let cliProfile = '';
    if(profile.length>0) cliProfile = profile;
    else cliProfile = state.profile;

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;

    let cliRuntime = '';
    if(runtime.length>0) cliRuntime = runtime;
    else cliRuntime = state.runtime.label;
    
    let cliExecutionRole = '';
    if(executionRole.length>0) cliExecutionRole = executionRole;
    else cliExecutionRole = state.role;
    
    let cliS3Bucket = '';
    if(boilerplateS3Bucket.length>0) cliS3Bucket = boilerplateS3Bucket;
    else cliS3Bucket = state.s3Bucket;
    
    let cliS3Key = '';
    if(boilerplateS3Key.length>0) cliS3Key = boilerplateS3Key;
    else cliS3Key = state.s3Key;

    let cliLayer = '';
    cliLayer = state.layer;
    let cliLayerVersion = '';
    cliLayerVersion = state.layerVersion;

    startBuild();
    
    async function startBuild(){
        //get accountID first with CLI

        const cmd = 'aws sts get-caller-identity --profile ' + cliProfile;
        //const { stdout, stderr } = execSync(cmd);
        const { stdout, stderr } = await exec(cmd);
        //console.log(cmd);
        console.log('stdout1:', stdout);
        console.log('stderr2:', stderr);

        const response = JSON.parse(stdout);
        const accountID = response.Account;
        
        cliAccountID = String(accountID);
    
        let cliCommand = 'aws lambda create-function --function-name "arn:aws:lambda:'+
            cliRegion+':'+cliAccountID+':function:'+cliFunctionName+'" --runtime "'+
            cliRuntime+'" --role "arn:aws:iam::'+cliAccountID+':role/'+cliExecutionRole+
            '" --handler "lambda_function.lambda_handler" --code "S3Bucket='+cliS3Bucket+
            ',S3Key='+cliS3Key+'"';
        //cliCommand += ' --layer "arn:aws:lambda:'+cliRegion+':'+cliAccountID+':layer:'+cliLayer+':'+cliLayerVersion+'"';
        cliCommand += ' --memory-size '+memory;
        cliCommand += ' --timeout '+timeout;
        cliCommand += ' --vpc-config SubnetIds='+subnet+',SecurityGroupIds='+securityGroup;
        let layerList = layers.split(',');
        let layerPrefix = 'arn:aws:lambda:'+region+':'+accountID+':layer:';
        let layerString = '';
        for(let i=0;i<layerList.length;i++){
            layerString +='"'+layerPrefix+layerList[i]+'"';
            if(i<layerList.length-1) layerString+=' ';
        }

        cliCommand += ' --layers '+layerString;
        cliCommand += ' --region '+cliRegion;
        cliCommand += ' --profile '+cliProfile;

        console.log(cliCommand);
        window.showInformationMessage(`Creating Lambda Function '${cliFunctionName}' with command ${cliCommand}`);
        

        //create aliases for lambda function
        buildLambdaFunction(cliCommand);
        
        return accountID;
    }

    /*
    async function executeCliCommand(cmd:string) {
        const { stdout, stderr } = await exec(cmd);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
    }
    */
    
    async function aliasCommand(cmd:string) {
        const { stdout, stderr } = await exec(cmd);
        console.log('stdout5:', stdout);
        console.log('stderr6:', stderr);
    }

    function buildLambdaFunction(cmd:string) {
        let lambdaAlias = ['Development','Production'];

        const { stdout, stderr } = execSync(cmd);
        console.log('stdout3:', stdout);
        console.log('stderr4:', stderr);

        //generate aliases only after main function is synchronously created
        for(let i=0;i<lambdaAlias.length;i++){
            let cliAliasCommand = 'aws lambda create-alias --function-name "arn:aws:lambda:'+
                cliRegion+':'+cliAccountID+':function:'+cliFunctionName+
                '" --name '+lambdaAlias[i]+' --function-version $LATEST';
                cliAliasCommand += ' --region '+cliRegion;
                cliAliasCommand += ' --profile '+cliProfile;

            window.showInformationMessage(`Creating Lambda Function Alias ${lambdaAlias[i]} for '${cliFunctionName}' with command ${cliAliasCommand}`);
            
            aliasCommand(cliAliasCommand);
            window.showInformationMessage(`Created Lambda Function Alias ${lambdaAlias[i]} for '${cliFunctionName}'`);
        }        
    }    
}

export async function multiStepUpdateLambdaFunction(context: ExtensionContext) {

    let settingsReducedSteps = 0;
	const title = 'Update AWS Lambda Function';
    const numSteps = 9;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const accountID = vscode.workspace.getConfiguration().get('conf.view.awsAccountID') as string;
    const executionRole = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole') as string;
    const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime') as string;
    const boilerplateS3Bucket = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Bucket') as string;
    const boilerplateS3Key = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Key') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;

    const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory') as string;
    const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout') as string;
    const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet') as string;
    const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup') as string;
    const layers = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers') as string;
    
    class MyButton implements QuickInputButton {
        constructor(public iconPath: { light: Uri; dark: Uri; }, public tooltip: string) { }
    }

    const createResourceGroupButton = new MyButton({
        dark: Uri.file(context.asAbsolutePath('resources/dark/add.svg')),
        light: Uri.file(context.asAbsolutePath('resources/light/add.svg')),
    }, 'Create Resource Group');

    const resourceGroups: QuickPickItem[] = ['vscode-data-function', 'vscode-appservice-microservices', 'vscode-appservice-monitor', 'vscode-appservice-preview', 'vscode-appservice-prod']
        .map(label => ({ label }));

    interface State {
		title: string;
		step: number;
		totalSteps: number;       
		functionName: string;
		runtime: QuickPickItem;
		role: string;
		layer: string;
		layerVersion: string;
		s3Bucket:string;
		s3Key:string;
		region: QuickPickItem;
		//accountID: string;
        profile: string;
        layers:string;
    }
    
    async function collectInputs() {
        

        const state = {} as Partial<State>;
        
        if(profile.length>0){
            settingsReducedSteps++;
        }
        if(executionRole.length>0){
            settingsReducedSteps++;
        }
        if(runtime.length>0){
            settingsReducedSteps++;
        }
        if(boilerplateS3Bucket.length>0){
            settingsReducedSteps++;
        }
        if(boilerplateS3Key.length>0){
            settingsReducedSteps++;
        }
        if(region.length>0){
            settingsReducedSteps++;
        }
        if(layers.length>0){
            settingsReducedSteps++;
            settingsReducedSteps++;
        }
        
        /*
        await MultiStepInput.run(input => pickResourceGroup(input, state));
        */
        await MultiStepInput.run(input => inputLambdaFunctionName(input, state));
        return state as State;
    }

    
    /*
    async function pickResourceGroup(input: MultiStepInput, state: Partial<State>) {
        const pick = await input.showQuickPick({
            title,
            step: 1,
            totalSteps: 3,
            placeholder: 'Pick a resource group',
            items: resourceGroups,
            activeItem: typeof state.resourceGroup !== 'string' ? state.resourceGroup : undefined,
            buttons: [createResourceGroupButton],
            shouldResume: shouldResume
        });
        if (pick instanceof MyButton) {
            return (input: MultiStepInput) => inputResourceGroupName(input, state);
        }
        state.resourceGroup = pick;
        return (input: MultiStepInput) => inputName(input, state);
    }
    */
    async function inputLambdaFunctionName(input: MultiStepInput, state: Partial<State>) {
        state.functionName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.functionName === 'string' ? state.functionName : '',
            prompt: 'Enter the name of an existing lambda function',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });

        if(runtime.length>0){
            if(executionRole.length>0){
                if(region.length>0){
                    if(profile.length>0){
                        if(layers.length>0){

                        }
                        else{
                            return (input: MultiStepInput) => inputLayer(input, state);
                        }
                        
                    }
                    else{
                        return (input: MultiStepInput) => inputProfile(input, state);
                    }
                }
                else{
                    return (input: MultiStepInput) => pickRegion(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => inputRole(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRuntime(input, state);
        }
            
    }

    async function pickRuntime(input: MultiStepInput, state: Partial<State>) {
        /*
        const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
        */
        const runtimes = await getAvailableRuntimes(state.runtime!, undefined /* TODO: token */);
        
        state.runtime = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a runtime',
            items: runtimes,
            activeItem: state.runtime,
            shouldResume: shouldResume
        });

        if(executionRole.length>0){
            if(region.length>0){
                if(profile.length>0){
                    return (input: MultiStepInput) => inputLayer(input, state);
                }
                else{
                    return (input: MultiStepInput) => inputProfile(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => pickRegion(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => inputRole(input, state);
        }
        
    }

    async function inputRole(input: MultiStepInput, state: Partial<State>) {
        const additionalSteps = typeof state.functionName === 'string' ? 1 : 0;
        // TODO: Remember current value when navigating back.
        state.role = await input.showInputBox({
            title,
            step: 3,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.role || '',
            prompt: 'Select an execution role',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            if(profile.length>0){
                return (input: MultiStepInput) => inputLayer(input, state);
            }
            else{
                return (input: MultiStepInput) => inputProfile(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        
	}
	
	async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        /*
        const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
        */
        const regions = await getAvailableRegions(state.region!, undefined /* TODO: token */);
        // TODO: Remember currently active item when navigating back.
        state.region = await input.showQuickPick({
            title,
            step: 4,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
        if(profile.length>0){
            return (input: MultiStepInput) => inputLayer(input, state);
        }
        else{
            return (input: MultiStepInput) => inputProfile(input, state);
        }
		
		
	}
	
	async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        
        state.profile = await input.showInputBox({
            title,
            step: 5,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.profile || '',
            prompt: 'Select an AWS profile (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
        return (input: MultiStepInput) => inputLayer(input, state);
        
	}

    //merge to this layer
	async function inputLayer(input: MultiStepInput, state: Partial<State>) {
        
        let currentStep = 6;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 2;
        }
        state.layer = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.layer || '',
            prompt: 'Pick a layer to add to your function (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        return (input: MultiStepInput) => inputLayerVersion(input, state);
        
	}

	async function inputLayerVersion(input: MultiStepInput, state: Partial<State>) {
        let currentStep = 7;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 2;
        }
        state.layerVersion = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.layerVersion || '',
            prompt: 'Pick a layer version',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(boilerplateS3Bucket.length>0){
            if(boilerplateS3Key.length>0){

            }
            else{
                return (input: MultiStepInput) => inputS3Key(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => inputS3Bucket(input, state);
        }
        
	}

	async function inputS3Bucket(input: MultiStepInput, state: Partial<State>) {
        
        state.s3Bucket = await input.showInputBox({
            title,
            step: 8,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.s3Bucket || '',
            prompt: 'Pick a S3 Bucket',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(boilerplateS3Key.length>0){

        }
        else{
            return (input: MultiStepInput) => inputS3Key(input, state);
        }
        
        
	}

	async function inputS3Key(input: MultiStepInput, state: Partial<State>) {
        
        state.s3Key = await input.showInputBox({
            title,
            step: 9,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.s3Key || '',
            prompt: 'Enter a S3 Key',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        /*
        return (input: MultiStepInput) => pickRegion(input, state);
        */
    }
    

    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	async function getAvailableRuntimes(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['python3.6']
			.map(label => ({ label }));
	}
	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    const util = require('util');
    //const execSync = util.promisify(require('child_process').execSync);
    const execSync = require('child_process').execSync;
    const exec = util.promisify(require('child_process').exec);

    let cliFunctionName = '';
    cliFunctionName = state.functionName;
    let cliAccountID = '';
    let cliProfile = '';
    if(profile.length>0) cliProfile = profile;
    else cliProfile = state.profile;

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;

    let cliRuntime = '';
    if(runtime.length>0) cliRuntime = runtime;
    else cliRuntime = state.runtime.label;
    
    let cliExecutionRole = '';
    if(executionRole.length>0) cliExecutionRole = executionRole;
    else cliExecutionRole = state.role;
    
    let cliS3Bucket = '';
    if(boilerplateS3Bucket.length>0) cliS3Bucket = boilerplateS3Bucket;
    else cliS3Bucket = state.s3Bucket;
    
    let cliS3Key = '';
    if(boilerplateS3Key.length>0) cliS3Key = boilerplateS3Key;
    else cliS3Key = state.s3Key;

    let cliLayer = '';
    cliLayer = state.layer;
    let cliLayerVersion = '';
    cliLayerVersion = state.layerVersion;

    startBuild();
    
    async function startBuild(){
        //get accountID first with CLI

        const cmd = 'aws sts get-caller-identity --profile ' + cliProfile;
        //const { stdout, stderr } = execSync(cmd);
        const { stdout, stderr } = await exec(cmd);
        //console.log(cmd);
        console.log('stdout1:', stdout);
        console.log('stderr2:', stderr);

        const response = JSON.parse(stdout);
        const accountID = response.Account;
        
        cliAccountID = String(accountID);
    
        let cliCommand = 'aws lambda update-function-configuration --function-name "arn:aws:lambda:'+
            cliRegion+':'+cliAccountID+':function:'+cliFunctionName+'" --runtime "'+
            cliRuntime+'" --role "arn:aws:iam::'+cliAccountID+':role/'+cliExecutionRole+'"';
        //cliCommand += ' --layer "arn:aws:lambda:'+cliRegion+':'+cliAccountID+':layer:'+cliLayer+':'+cliLayerVersion+'"';
        cliCommand += ' --memory-size '+memory;
        cliCommand += ' --timeout '+timeout;
        cliCommand += ' --vpc-config SubnetIds='+subnet+',SecurityGroupIds='+securityGroup;
        let layerList = layers.split(',');
        let layerPrefix = 'arn:aws:lambda:'+region+':'+accountID+':layer:';
        let layerString = '';
        for(let i=0;i<layerList.length;i++){
            layerString +='"'+layerPrefix+layerList[i]+'"';
            if(i<layerList.length-1) layerString+=' ';
        }

        cliCommand += ' --layers '+layerString;
        cliCommand += ' --region '+cliRegion;
        cliCommand += ' --profile '+cliProfile;

        console.log(cliCommand);
        window.showInformationMessage(`Updating Lambda Function '${cliFunctionName}' with command ${cliCommand}`);
        const { stdout3, stderr4 } = execSync(cliCommand);
        console.log('stdout3:', stdout3);
        console.log('stderr4:', stderr4);

        window.showInformationMessage(`Updated Lambda Function '${cliFunctionName}'`);

        return accountID;
    }

}

export async function multiStepCreateLambdaFunctionCRUD(context: ExtensionContext) {

    let settingsReducedSteps = 0;
	const title = 'Create AWS Lambda CRUD Functions';
    const numSteps = 9;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const executionRole = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole') as string;
    const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime') as string;
    const boilerplateS3Bucket = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Bucket') as string;
    const boilerplateS3Key = vscode.workspace.getConfiguration().get('conf.view.awsLambdaBoilerplateS3Key') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;

    const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory') as string;
    const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout') as string;
    const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet') as string;
    const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup') as string;
    const layers = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers') as string;
    
    class MyButton implements QuickInputButton {
        constructor(public iconPath: { light: Uri; dark: Uri; }, public tooltip: string) { }
    }

    const createResourceGroupButton = new MyButton({
        dark: Uri.file(context.asAbsolutePath('resources/dark/add.svg')),
        light: Uri.file(context.asAbsolutePath('resources/light/add.svg')),
    }, 'Create Resource Group');

    const resourceGroups: QuickPickItem[] = ['vscode-data-function', 'vscode-appservice-microservices', 'vscode-appservice-monitor', 'vscode-appservice-preview', 'vscode-appservice-prod']
        .map(label => ({ label }));

    interface State {
		title: string;
		step: number;
		totalSteps: number;       
		functionBaseName: string;
		runtime: QuickPickItem;
		role: string;
		layer: string;
		layerVersion: string;
		s3Bucket:string;
		s3Key:string;
		region: QuickPickItem;
        profile: string;
        layers:string;
    }
    
    async function collectInputs() {

        const state = {} as Partial<State>;
        
        if(profile.length>0){
            settingsReducedSteps++;
        }
        if(executionRole.length>0){
            settingsReducedSteps++;
        }
        if(runtime.length>0){
            settingsReducedSteps++;
        }
        if(boilerplateS3Bucket.length>0){
            settingsReducedSteps++;
        }
        if(boilerplateS3Key.length>0){
            settingsReducedSteps++;
        }
        if(region.length>0){
            settingsReducedSteps++;
        }
        if(layers.length>0){
            settingsReducedSteps++;
            settingsReducedSteps++;
        }
        
        await MultiStepInput.run(input => inputLambdaFunctionBaseName(input, state));
        return state as State;
    }

    async function inputLambdaFunctionBaseName(input: MultiStepInput, state: Partial<State>) {
        state.functionBaseName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.functionBaseName === 'string' ? state.functionBaseName : '',
            prompt: 'Choose a unique base name for the lambda function',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });

        if(runtime.length>0){
            if(executionRole.length>0){
                if(region.length>0){
                    if(profile.length>0){
                        if(layers.length>0){

                        }
                        else{
                            return (input: MultiStepInput) => inputLayer(input, state);
                        }
                    }
                    else{
                        return (input: MultiStepInput) => inputProfile(input, state);
                    }
                }
                else{
                    return (input: MultiStepInput) => pickRegion(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => inputRole(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRuntime(input, state);
        }
            
    }

    async function pickRuntime(input: MultiStepInput, state: Partial<State>) {
        /*
        const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
        */
        const runtimes = await getAvailableRuntimes(state.runtime!, undefined /* TODO: token */);
        
        state.runtime = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a runtime',
            items: runtimes,
            activeItem: state.runtime,
            shouldResume: shouldResume
        });

        if(executionRole.length>0){
            if(region.length>0){
                if(profile.length>0){
                    return (input: MultiStepInput) => inputLayer(input, state);
                }
                else{
                    return (input: MultiStepInput) => inputProfile(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => pickRegion(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => inputRole(input, state);
        }
        
    }

    async function inputRole(input: MultiStepInput, state: Partial<State>) {
        
        state.role = await input.showInputBox({
            title,
            step: 3,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.role || '',
            prompt: 'Select an execution role',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            if(profile.length>0){
                return (input: MultiStepInput) => inputLayer(input, state);
            }
            else{
                return (input: MultiStepInput) => inputProfile(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        
	}
	
	async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined /* TODO: token */);
        
        state.region = await input.showQuickPick({
            title,
            step: 4,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
        if(profile.length>0){
            return (input: MultiStepInput) => inputLayer(input, state);
        }
        else{
            return (input: MultiStepInput) => inputProfile(input, state);
        }
		
		
	}
	
	async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        
        state.profile = await input.showInputBox({
            title,
            step: 5,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.profile || '',
            prompt: 'Select an AWS profile (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
        return (input: MultiStepInput) => inputLayer(input, state);
        
	}

    //merge to this layer
	async function inputLayer(input: MultiStepInput, state: Partial<State>) {
        
        let currentStep = 6;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 2;
        }
        state.layer = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.layer || '',
            prompt: 'Pick a layer to add to your function (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        return (input: MultiStepInput) => inputLayerVersion(input, state);
        
	}

	async function inputLayerVersion(input: MultiStepInput, state: Partial<State>) {
        let currentStep = 7;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 2;
        }
        state.layerVersion = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.layerVersion || '',
            prompt: 'Pick a layer version',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(boilerplateS3Bucket.length>0){
            if(boilerplateS3Key.length>0){

            }
            else{
                return (input: MultiStepInput) => inputS3Key(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => inputS3Bucket(input, state);
        }
        
	}

	async function inputS3Bucket(input: MultiStepInput, state: Partial<State>) {
        
        state.s3Bucket = await input.showInputBox({
            title,
            step: 8,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.s3Bucket || '',
            prompt: 'Pick a S3 Bucket',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(boilerplateS3Key.length>0){

        }
        else{
            return (input: MultiStepInput) => inputS3Key(input, state);
        }
        
        
	}

	async function inputS3Key(input: MultiStepInput, state: Partial<State>) {
        
        state.s3Key = await input.showInputBox({
            title,
            step: 9,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.s3Key || '',
            prompt: 'Enter a S3 Key',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        /*
        return (input: MultiStepInput) => pickRegion(input, state);
        */
    }
    

    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	async function getAvailableRuntimes(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['python3.6']
			.map(label => ({ label }));
	}
	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    const util = require('util');
    const execSync = util.promisify(require('child_process').execSync);
    const exec = util.promisify(require('child_process').exec);

    let cliFunctionBaseName = '';
    cliFunctionBaseName = state.functionBaseName;
    let cliFunctionName = '';
    
    let cliAccountID = '';

    let cliProfile = '';
    if(profile.length>0) cliProfile = profile;
    else cliProfile = state.profile;
    
    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;

    let cliRuntime = '';
    if(runtime.length>0) cliRuntime = runtime;
    else cliRuntime = state.runtime.label;
    
    let cliExecutionRole = '';
    if(executionRole.length>0) cliExecutionRole = executionRole;
    else cliExecutionRole = state.role;

    let cliS3Bucket = '';
    if(boilerplateS3Bucket.length>0) cliS3Bucket = boilerplateS3Bucket;
    else cliS3Bucket = state.s3Bucket;
    
    let cliS3Key = '';
    if(boilerplateS3Key.length>0) cliS3Key = boilerplateS3Key;
    else cliS3Key = state.s3Key;

    let cliLayer = '';
    cliLayer = state.layer;
    let cliLayerVersion = '';
    cliLayerVersion = state.layerVersion;

    startBuild();

    async function startBuild(){
        const { stdout, stderr } = await exec('aws sts get-caller-identity --profile '+cliProfile);
        //console.log('stdout:', stdout);
        //console.log('stderr:', stderr);

        const response = JSON.parse(stdout);
        const accountID = response.Account;
        
        cliAccountID = String(accountID);

        let cliCommand = '';
        
        let crudPrefix = ['Add','Update','Query','Get','Bulk','Empty','Delete'];

        for(let i=0;i<crudPrefix.length;i++){
            cliFunctionName = crudPrefix[i]+cliFunctionBaseName;
            cliCommand = 'aws lambda create-function  --function-name "arn:aws:lambda:'+
                cliRegion+':'+cliAccountID+':function:'+cliFunctionName+'" --runtime "'+
                cliRuntime+'" --role "arn:aws:iam::'+cliAccountID+':role/'+cliExecutionRole+
                '" --handler "lambda_function.lambda_handler" --code "S3Bucket='+cliS3Bucket+
                ',S3Key='+cliS3Key+'"';
            //cliCommand += ' --layer "arn:aws:lambda:'+cliRegion+':'+cliAccountID+':layer:'+cliLayer+':'+cliLayerVersion+'"';
            cliCommand += ' --memory-size '+memory;
            cliCommand += ' --timeout '+timeout;
            cliCommand += ' --vpc-config SubnetIds='+subnet+',SecurityGroupIds='+securityGroup;
            let layerList = layers.split(',');
            let layerPrefix = 'arn:aws:lambda:'+region+':'+accountID+':layer:';
            let layerString = '';
            for(let i=0;i<layerList.length;i++){
                layerString +='"'+layerPrefix+layerList[i]+'"';
                if(i<layerList.length-1) layerString+=' ';
            }

            cliCommand += ' --layers '+layerString;
            cliCommand += ' --region '+cliRegion;
            cliCommand += ' --profile '+cliProfile;

            window.showInformationMessage(`Creating Lambda Function '${cliFunctionName}' with command ${cliCommand}`);
            buildLambdaFunction(cliCommand);
            
        }
        
        return accountID;
    }
    
    
    
    async function aliasCommand(cmd:string) {
        const { stdout, stderr } = await exec(cmd);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
    }
    function buildLambdaFunction(cmd:string) {
        let lambdaAlias = ['Development','Production'];

        const { stdout, stderr } = execSync(cmd);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
        window.showInformationMessage(`Created Lambda Function '${cliFunctionName}'`);

        //generate aliases only after main function is synchronously created
        for(let i=0;i<lambdaAlias.length;i++){
            
            let cliAliasCommand = 'aws lambda create-alias --function-name "arn:aws:lambda:'+
                cliRegion+':'+cliAccountID+':function:'+cliFunctionName+
                '" --name '+lambdaAlias[i]+' --function-version $LATEST';
                cliAliasCommand += ' --region '+cliRegion;
                cliAliasCommand += ' --profile '+cliProfile;
            
            window.showInformationMessage(`Creating Lambda Function Alias ${lambdaAlias[i]} for '${cliFunctionName}' with command ${cliAliasCommand}`);
            aliasCommand(cliAliasCommand);
            window.showInformationMessage(`Created Lambda Function Alias ${lambdaAlias[i]} for '${cliFunctionName}'`);
        }        
    }


}

export async function multiStepUpdateLambdaFunctionCRUD(context: ExtensionContext) {

    let settingsReducedSteps = 0;
	const title = 'Update AWS Lambda CRUD Functions';
    const numSteps = 7;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const executionRole = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole') as string;
    const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;

    const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory') as string;
    const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout') as string;
    const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet') as string;
    const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup') as string;
    const layers = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers') as string;
    
    class MyButton implements QuickInputButton {
        constructor(public iconPath: { light: Uri; dark: Uri; }, public tooltip: string) { }
    }

    const createResourceGroupButton = new MyButton({
        dark: Uri.file(context.asAbsolutePath('resources/dark/add.svg')),
        light: Uri.file(context.asAbsolutePath('resources/light/add.svg')),
    }, 'Create Resource Group');

    const resourceGroups: QuickPickItem[] = ['vscode-data-function', 'vscode-appservice-microservices', 'vscode-appservice-monitor', 'vscode-appservice-preview', 'vscode-appservice-prod']
        .map(label => ({ label }));

    interface State {
		title: string;
		step: number;
		totalSteps: number;       
		functionBaseName: string;
		runtime: QuickPickItem;
		role: string;
		layer: string;
		layerVersion: string;
		region: QuickPickItem;
        profile: string;
        layers:string;
    }
    
    async function collectInputs() {

        const state = {} as Partial<State>;
        
        if(profile.length>0){
            settingsReducedSteps++;
        }
        if(executionRole.length>0){
            settingsReducedSteps++;
        }
        if(runtime.length>0){
            settingsReducedSteps++;
        }
        if(region.length>0){
            settingsReducedSteps++;
        }
        if(layers.length>0){
            settingsReducedSteps++;
            settingsReducedSteps++;
        }
        
        await MultiStepInput.run(input => inputLambdaFunctionBaseName(input, state));
        return state as State;
    }

    async function inputLambdaFunctionBaseName(input: MultiStepInput, state: Partial<State>) {
        state.functionBaseName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.functionBaseName === 'string' ? state.functionBaseName : '',
            prompt: 'Choose an existing base name for the lambda functions to update',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });

        if(runtime.length>0){
            if(executionRole.length>0){
                if(region.length>0){
                    if(profile.length>0){
                        if(layers.length>0){

                        }
                        else{
                            return (input: MultiStepInput) => inputLayer(input, state);
                        }
                    }
                    else{
                        return (input: MultiStepInput) => inputProfile(input, state);
                    }
                }
                else{
                    return (input: MultiStepInput) => pickRegion(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => inputRole(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRuntime(input, state);
        }
            
    }

    async function pickRuntime(input: MultiStepInput, state: Partial<State>) {
        /*
        const additionalSteps = typeof state.resourceGroup === 'string' ? 1 : 0;
        */
        const runtimes = await getAvailableRuntimes(state.runtime!, undefined /* TODO: token */);
        
        state.runtime = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a runtime',
            items: runtimes,
            activeItem: state.runtime,
            shouldResume: shouldResume
        });

        if(executionRole.length>0){
            if(region.length>0){
                if(profile.length>0){
                    return (input: MultiStepInput) => inputLayer(input, state);
                }
                else{
                    return (input: MultiStepInput) => inputProfile(input, state);
                }
            }
            else{
                return (input: MultiStepInput) => pickRegion(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => inputRole(input, state);
        }
        
    }

    async function inputRole(input: MultiStepInput, state: Partial<State>) {
        
        state.role = await input.showInputBox({
            title,
            step: 3,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.role || '',
            prompt: 'Select an execution role',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            if(profile.length>0){
                return (input: MultiStepInput) => inputLayer(input, state);
            }
            else{
                return (input: MultiStepInput) => inputProfile(input, state);
            }
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        
	}
	
	async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined /* TODO: token */);
        
        state.region = await input.showQuickPick({
            title,
            step: 4,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
        if(profile.length>0){
            return (input: MultiStepInput) => inputLayer(input, state);
        }
        else{
            return (input: MultiStepInput) => inputProfile(input, state);
        }
		
		
	}
	
	async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        
        state.profile = await input.showInputBox({
            title,
            step: 5,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.profile || '',
            prompt: 'Select an AWS profile (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
        return (input: MultiStepInput) => inputLayer(input, state);
        
	}

    //merge to this layer
	async function inputLayer(input: MultiStepInput, state: Partial<State>) {
        
        let currentStep = 6;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 2;
        }
        state.layer = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.layer || '',
            prompt: 'Pick a layer to add to your function (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        return (input: MultiStepInput) => inputLayerVersion(input, state);
        
	}

	async function inputLayerVersion(input: MultiStepInput, state: Partial<State>) {
        let currentStep = 7;
        if(settingsReducedSteps>0){
            currentStep = currentStep - settingsReducedSteps + 2;
        }
        state.layerVersion = await input.showInputBox({
            title,
            step: currentStep,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.layerVersion || '',
            prompt: 'Pick a layer version',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
                
	}


    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	async function getAvailableRuntimes(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['python3.6']
			.map(label => ({ label }));
	}
	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    const util = require('util');
    const execSync = util.promisify(require('child_process').execSync);
    const exec = util.promisify(require('child_process').exec);

    let cliFunctionBaseName = '';
    cliFunctionBaseName = state.functionBaseName;
    let cliFunctionName = '';
    
    let cliAccountID = '';

    let cliProfile = '';
    if(profile.length>0) cliProfile = profile;
    else cliProfile = state.profile;
    
    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;

    let cliRuntime = '';
    if(runtime.length>0) cliRuntime = runtime;
    else cliRuntime = state.runtime.label;
    
    let cliExecutionRole = '';
    if(executionRole.length>0) cliExecutionRole = executionRole;
    else cliExecutionRole = state.role;

    let cliLayer = '';
    cliLayer = state.layer;
    let cliLayerVersion = '';
    cliLayerVersion = state.layerVersion;

    startBuild();

    async function startBuild(){
        const { stdout, stderr } = await exec('aws sts get-caller-identity --profile '+cliProfile);
        //console.log('stdout:', stdout);
        //console.log('stderr:', stderr);

        const response = JSON.parse(stdout);
        const accountID = response.Account;
        
        cliAccountID = String(accountID);

        let cliCommand = '';
        
        let crudPrefix = ['Add','Update','Query','Get','Bulk','Empty','Delete'];

        for(let i=0; i<crudPrefix.length; i++){
            cliFunctionName = crudPrefix[i]+cliFunctionBaseName;
            let cliCommand = 'aws lambda update-function-configuration --function-name "arn:aws:lambda:'+
            cliRegion+':'+cliAccountID+':function:'+cliFunctionName+'" --runtime "'+
            cliRuntime+'" --role "arn:aws:iam::'+cliAccountID+':role/'+cliExecutionRole+'"';
            //cliCommand += ' --layer "arn:aws:lambda:'+cliRegion+':'+cliAccountID+':layer:'+cliLayer+':'+cliLayerVersion+'"';
            cliCommand += ' --memory-size '+memory;
            cliCommand += ' --timeout '+timeout;
            cliCommand += ' --vpc-config SubnetIds='+subnet+',SecurityGroupIds='+securityGroup;
            let layerList = layers.split(',');
            let layerPrefix = 'arn:aws:lambda:'+region+':'+accountID+':layer:';
            let layerString = '';
            for(let i=0; i<layerList.length; i++){
                layerString += '"' + layerPrefix + layerList[i] + '"';
                if(i<layerList.length-1) layerString += ' ';
            }

            cliCommand += ' --layers '+layerString;
            cliCommand += ' --region '+cliRegion;
            cliCommand += ' --profile '+cliProfile;
            
            window.showInformationMessage(`Updating Lambda Function '${cliFunctionName}' with command ${cliCommand}`);
            updateLambdaFunction(cliCommand);
            
        }
        
        return accountID;
    }
    
    function updateLambdaFunction(cmd:string) {
        const { stdout, stderr } = execSync(cmd);
        console.log('stdout:', stdout);
        console.log('stderr:', stderr);
        window.showInformationMessage(`Updated Lambda Function '${cliFunctionName}'`);
    }


}

export async function multiStepUpdateLambdaLayerVersion(context:ExtensionContext){
    
    let settingsReducedSteps = 0;
	const title = 'Update Lambda Layer Version';
    const numSteps = 7;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const layerName = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayer') as string;
    const runtime = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRuntime') as string;
    const folder = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayerFolder') as string;
    const tempFilename = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayerTempZipFilename') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        layerName: string;
        runtime: QuickPickItem;
        folder: string;
        tempFilename: string;
        description: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(profile.length>0) settingsReducedSteps++;
        if(region.length>0) settingsReducedSteps++;
        if(layerName.length>0) settingsReducedSteps++;
        if(runtime.length>0) settingsReducedSteps++;
        if(folder.length>0) settingsReducedSteps++;
        if(tempFilename.length>0) settingsReducedSteps++;        
        
        await MultiStepInput.run(input => inputLambdaLayerVersionDescription(input, state));
        return state as State;
    }

    async function inputLambdaLayerVersionDescription(input: MultiStepInput, state: Partial<State>) {
        state.description = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.description === 'string' ? state.description : '',
            prompt: 'Enter a description for the new layer version.',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });

        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }

    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
        if(profile.length>0){
            
        }
        else{
            return (input: MultiStepInput) => inputProfile(input, state);
        }
		
		
	}
	
	async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        
        state.profile = await input.showInputBox({
            title,
            step: 3,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.profile || '',
            prompt: 'Select an AWS profile (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
    }
    
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	async function getAvailableRuntimes(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['python3.6']
			.map(label => ({ label }));
	}
	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    const util = require('util');
    const exec = util.promisify(require('child_process').exec);
    const execSync = require('child_process').execSync;

    let cliLayerVersionDescription = '';
    cliLayerVersionDescription = state.description;
    
    //cliFunctionAlias = state.functionAlias;
    
    let cliProfile = '';
    if(profile.length>0) cliProfile = profile;
    else cliProfile = state.profile;

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    let cliLambdaLayerName = '';
    if(layerName.length>0) cliLambdaLayerName = layerName;

    let cliRuntime = '';
    if(runtime.length>0) cliRuntime = runtime;

    let cliLambdaLayerVersionDescription = '';
    if(state.description.length>0) cliLambdaLayerVersionDescription = state.description;

    let cliLambdaLayerFolder = '';
    if(folder.length>0) cliLambdaLayerFolder = folder;

    let cliLambdaLayerTempZipFilename = '';
    if(tempFilename.length>0) cliLambdaLayerTempZipFilename = tempFilename;

    startBuild();

    function delay(ms: number) {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

    async function startBuild(){
        const { stdout, stderr } = await exec('aws sts get-caller-identity --profile '+profile);
        //console.log('stdout:', stdout);
        //console.log('stderr:', stderr);
        let cliLambdaLayerTempZipFilenamePath = cliLambdaLayerFolder+'\\..\\'+cliLambdaLayerTempZipFilename;

        let delCommand = 'del '+cliLambdaLayerTempZipFilenamePath;
        //window.showInformationMessage(delCommand);
        
        
        try {
            const { stdoutA, stderrB } = execSync(delCommand);
        }
        catch(err){
            console.log(cliLambdaLayerFolder+'\\..\\'+cliLambdaLayerTempZipFilename+' does not exist.');
        }
        window.showInformationMessage(`Compressing '${cliLambdaLayerFolder}' into ${cliLambdaLayerTempZipFilenamePath}`);
        //tar zip the folder        
        let zipCommand = 'tar.exe -C "'+cliLambdaLayerFolder+'" -acf "'+cliLambdaLayerTempZipFilenamePath+'" .';
        const { stdoutY, stderrZ } = execSync(zipCommand);

        const response = JSON.parse(stdout);
        const accountID = response.Account;

        let cliCommand = 'aws lambda publish-layer-version --layer-name "' + cliLambdaLayerName + '" --description "'+
            cliLambdaLayerVersionDescription+'" --compatible-runtimes '+ cliRuntime + 
            ' --zip-file fileb://' + cliLambdaLayerTempZipFilenamePath;

        cliCommand += ' --region '+cliRegion;
        cliCommand += ' --profile '+cliProfile;

        window.showInformationMessage(`Adding new lambda layer '${cliLambdaLayerName}' with '${cliCommand}'`);

        addLambdaLayerVersion(cliCommand, cliLambdaLayerName, cliLambdaLayerTempZipFilenamePath);
        
        return accountID;
    }
    
    async function addLambdaLayerVersion(cmd:string, layerName:string, tempZipFilename:string) {
        const { stdout, stderr } = await exec(cmd);
        console.log('stdout:', stdout);
        //console.log('stderr:', stderr);
        let resp = JSON.parse(stdout);

        const { stdoutE, stderrF } = execSync('del '+tempZipFilename);
        let version = resp.Version;

        window.showInformationMessage(`Added new lambda layer '${layerName}:${version}'.`);
    }
    
    
}

export async function multiStepUpdateLambdaFunctionLayer(context:ExtensionContext){
    
}

export async function multiStepDeployProductionLambdaFunction(context:ExtensionContext){
    
    let settingsReducedSteps = 0;
	const title = 'Deploy Production Lambda Function';
    const numSteps = 4;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    
    let functionName = '';
    let description = '';
    
    //need function name
    //need description
    //alias default to Production
    //get version number after deploy

    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        functionName: string;
        description: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(profile.length>0) settingsReducedSteps++;
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputLambdaFunctionName(input, state));
        return state as State;
    }

    async function inputLambdaFunctionName(input: MultiStepInput, state: Partial<State>) {
        state.functionName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.functionName === 'string' ? state.functionName : '',
            prompt: 'Enter a lambda function name.',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        functionName = state.functionName;

        if(description.length>0){
            
        }
        else{
            return (input: MultiStepInput) => inputLambdaLayerVersionDescription(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }

    async function inputLambdaLayerVersionDescription(input: MultiStepInput, state: Partial<State>) {
        state.description = await input.showInputBox({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.description === 'string' ? state.description : '',
            prompt: 'Enter a description for the new lambda function version.',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });

        description = state.description;

        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }

    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
        if(profile.length>0){
            
        }
        else{
            return (input: MultiStepInput) => inputProfile(input, state);
        }
		
		
	}
	
	async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        
        state.profile = await input.showInputBox({
            title,
            step: 3,
            totalSteps: numSteps-settingsReducedSteps,
            value: state.profile || '',
            prompt: 'Select an AWS profile (optional)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        
    }
    
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	async function getAvailableRuntimes(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['python3.6']
			.map(label => ({ label }));
	}
	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    const util = require('util');
    const exec = util.promisify(require('child_process').exec);
    const execSync = require('child_process').execSync;

    
    let cliLambdaFunctionName = '';
    if(state.functionName.length>0) cliLambdaFunctionName = state.functionName;


    let cliLambdaFunctionVersionDescription = '';
    cliLambdaFunctionVersionDescription = state.description;
    
    //cliFunctionAlias = state.functionAlias;
    
    let cliProfile = '';
    if(profile.length>0) cliProfile = profile;
    else cliProfile = state.profile;

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    
    startBuild();

    async function startBuild(){
        const { stdout, stderr } = await exec('aws sts get-caller-identity --profile '+profile);
        //console.log('stdout:', stdout);
        //console.log('stderr:', stderr);
        const response = JSON.parse(stdout);
        const accountID = response.Account;

        window.showInformationMessage(`Deploying new '${cliLambdaFunctionName}' version with description '${cliLambdaFunctionVersionDescription}'`);
        
        //deploy version
        let versionCommand = 'aws lambda publish-version --function-name "'+ cliLambdaFunctionName +'" --description "'+ cliLambdaFunctionVersionDescription +'"';
        versionCommand += ' --region '+cliRegion;
        versionCommand += ' --profile '+cliProfile;
        
        addLambdaFunctionVersion(versionCommand, cliLambdaFunctionName);

        
        return accountID;
    }

    async function addLambdaFunctionVersion(cmd:string, functionName:string) {
        const { stdout, stderr } = await exec(cmd);
        console.log('stdout:', stdout);
        //console.log('stderr:', stderr);
        let resp = JSON.parse(stdout);

        let version = resp.Version;

        window.showInformationMessage('Added new lambda function version '+version+' for '+functionName);
        
        let cliCommand = 'aws lambda update-alias --function-name "'+functionName+'" --name "Production" --function-version '+version;

        cliCommand += ' --region '+cliRegion;
        cliCommand += ' --profile '+cliProfile;
        
        updateProductionLambdaVersion(cliCommand,functionName, version);
    }
    
    async function updateProductionLambdaVersion(cmd:string, functionName:string, versionNumber:string) {
        const { stdout, stderr } = await exec(cmd);
        console.log('stdout:', stdout);
        //console.log('stderr:', stderr);
        let resp = JSON.parse(stdout);

        window.showInformationMessage('Deployed new lambda function "'+functionName+'" with version '+versionNumber+' to Production.');
    }
}

export async function multiStepOpenLambdaFunctionAWSConsole(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Deploy Production Lambda Function';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    
    let functionName = '';
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        functionName: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputLambdaFunctionName(input, state));
        return state as State;
    }

    async function inputLambdaFunctionName(input: MultiStepInput, state: Partial<State>) {
        state.functionName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.functionName === 'string' ? state.functionName : '',
            prompt: 'Enter a lambda function name.',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        functionName = state.functionName;

        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
		
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        //const { stdout, stderr } = await exec('aws sts get-caller-identity --profile '+profile);
        //console.log('stdout:', stdout);
        //console.log('stderr:', stderr);
        //const response = JSON.parse(stdout);
        //const accountID = response.Account;

        let url = 'https://'+region+'.console.aws.amazon.com/lambda/home?region='+region+'#/functions/'+functionName+'/versions/$LATEST?tab=configuration';
        
        vscode.env.openExternal(vscode.Uri.parse(url));

        
        //return accountID;
        return null;
    }

    
}

export async function multiStepUpdateSettingsVPC(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS VPC Settings';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const vpc = vscode.workspace.getConfiguration().get('conf.view.awsVPC') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        vpcId: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputVPC(input, state));
        return state as State;
    }

    async function inputVPC(input: MultiStepInput, state: Partial<State>) {
        state.vpcId = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.vpcId === 'string' ? state.vpcId : vpc,
            prompt: 'Enter VPC Identifier',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        

        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
        
		
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsVPC',state.vpcId, true);
        const vpc = vscode.workspace.getConfiguration().get('conf.view.awsVPC') as string;
        updateStatusBarVPCItem();
        window.showInformationMessage('VPC Identifier Updated.');
        return null;
    }

    
}

export async function multiStepUpdateSettingsProfile(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS Profile';
    const numSteps = 1;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        await MultiStepInput.run(input => inputProfile(input, state));
        return state as State;
    }

    async function inputProfile(input: MultiStepInput, state: Partial<State>) {
        state.profile = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.profile === 'string' ? state.profile : profile,
            prompt: 'Enter AWS Profile',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
    }
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}


    const state = await collectInputs();
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsProfile',state.profile, true);
        const vpc = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
        updateStatusBarProfileItem();
        window.showInformationMessage('AWS Profile Updated.');
        return null;
    }

    
}

export async function multiStepUpdateSettingsSubnet(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS VPC Subnet Settings';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        subnet: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputSubnet(input, state));
        return state as State;
    }

    async function inputSubnet(input: MultiStepInput, state: Partial<State>) {
        state.subnet = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.subnet === 'string' ? state.subnet : subnet,
            prompt: 'Enter comma separated subnet list',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsVPCSubnet',state.subnet, true);
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSubnet') as string;
        updateStatusBarVPCSubnetItem();
        window.showInformationMessage('VPC Subnet Updated.');
        return null;
    }

    
}

export async function multiStepUpdateSettingsSecurityGroup(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS VPC Security Group Settings';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const securityGroup = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        securityGroup: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputSecurityGroup(input, state));
        return state as State;
    }

    async function inputSecurityGroup(input: MultiStepInput, state: Partial<State>) {
        state.securityGroup = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.securityGroup === 'string' ? state.securityGroup : securityGroup,
            prompt: 'Enter comma separated security group list',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsVPCSecurityGroup',state.securityGroup, true);
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsVPCSecurityGroup') as string;
        updateStatusBarVPCSecurityGroupItem();
        window.showInformationMessage('VPC Security Group Updated.');
        return null;
    }

    
}

export async function multiStepUpdateSettingsLambdaLayer(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS Lambda Layer';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const layerName = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayer') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        layerName: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputLayerName(input, state));
        return state as State;
    }

    async function inputLayerName(input: MultiStepInput, state: Partial<State>) {
        state.layerName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.layerName === 'string' ? state.layerName : layerName,
            prompt: 'Enter lambda layer name',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsLambdaLayer',state.layerName, true);
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayer') as string;
        updateStatusBarLayerItem();
        window.showInformationMessage('Lambda Layer Name Updated.');
        return null;
    }
}

export async function multiStepUpdateSettingsLambdaLayers(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS Lambda Layer';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const layersName = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        layersName: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputLayerName(input, state));
        return state as State;
    }

    async function inputLayerName(input: MultiStepInput, state: Partial<State>) {
        state.layersName = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.layersName === 'string' ? state.layersName : layersName,
            prompt: 'Enter comma separated lambda layer arn list',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsLambdaLayers',state.layersName, true);
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaLayers') as string;
        updateStatusBarLayersItem();
        window.showInformationMessage('Lambda Layers ARN Updated.');
        return null;
    }
}

export async function multiStepUpdateSettingsLambdaMemory(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS Lambda Memory Settings';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const memory = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        memory: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputLayerName(input, state));
        return state as State;
    }

    async function inputLayerName(input: MultiStepInput, state: Partial<State>) {
        state.memory = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.memory === 'string' ? state.memory : memory,
            prompt: 'Enter memory amount (MB)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsLambdaMemory',state.memory, true);
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory') as string;
        updateStatusBarMemoryItem();
        window.showInformationMessage('Lambda Memory Updated.');
        return null;
    }
}

export async function multiStepUpdateSettingsLambdaTimeout(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS Lambda Timeout Settings';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const timeout = vscode.workspace.getConfiguration().get('conf.view.awsLambdaMemory') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        timeout: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputTimeout(input, state));
        return state as State;
    }

    async function inputTimeout(input: MultiStepInput, state: Partial<State>) {
        state.timeout = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.timeout === 'string' ? state.timeout : timeout,
            prompt: 'Enter timeout amount (s)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsLambdaTimeout',state.timeout, true);
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaTimeout') as string;
        updateStatusBarTimeoutItem();
        window.showInformationMessage('Lambda Timeout Updated.');
        return null;
    }
}

export async function multiStepUpdateSettingsExecutionRole(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS Lambda Execution Role Settings';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const role = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        role: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputTimeout(input, state));
        return state as State;
    }

    async function inputTimeout(input: MultiStepInput, state: Partial<State>) {
        state.role = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.role === 'string' ? state.role : role,
            prompt: 'Enter timeout amount (s)',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsLambdaExecutionRole',state.role, true);
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsLambdaExecutionRole') as string;
        updateStatusBarRoleItem();
        window.showInformationMessage('Lambda Execution Role Updated.');
        return null;
    }
}

export async function multiStepUpdateSettingsAPIGateway(context:ExtensionContext){

    let settingsReducedSteps = 0;
	const title = 'Update AWS API Gateway Settings';
    const numSteps = 2;
    
    const profile = vscode.workspace.getConfiguration().get('conf.view.awsProfile') as string;
    const region = vscode.workspace.getConfiguration().get('conf.view.awsLambdaRegion') as string;
    const gateway = vscode.workspace.getConfiguration().get('conf.view.awsAPIGatewayID') as string;
    
    
    interface State {
		title: string;
		step: number;
        totalSteps: number;       
        region: QuickPickItem;
		profile: string;
        gateway: string;
    }

    async function collectInputs() {
        
        const state = {} as Partial<State>;
        
        if(region.length>0) settingsReducedSteps++;
        
        await MultiStepInput.run(input => inputTimeout(input, state));
        return state as State;
    }

    async function inputTimeout(input: MultiStepInput, state: Partial<State>) {
        state.gateway = await input.showInputBox({
            title,
            step: 1,
            totalSteps: numSteps-settingsReducedSteps,
            value: typeof state.gateway === 'string' ? state.gateway : gateway,
            prompt: 'Enter Gateway ID',
            validate: validateNameIsUnique,
            shouldResume: shouldResume
        });
        
        if(region.length>0){
            
        }
        else{
            return (input: MultiStepInput) => pickRegion(input, state);
        }
        
        //return (input: MultiStepInput) => inputAlias(input, state);
    }


    async function pickRegion(input: MultiStepInput, state: Partial<State>) {
        
        const regions = await getAvailableRegions(state.region!, undefined);
        
        state.region = await input.showQuickPick({
            title,
            step: 2,
            totalSteps: numSteps-settingsReducedSteps,
            placeholder: 'Pick a region for your function',
            items: regions,
            activeItem: state.region,
            shouldResume: shouldResume
		});
	}
    
    function shouldResume() {
        // Could show a notification with the option to resume.
        return new Promise<boolean>((resolve, reject) => {

        });
    }

	async function validateNameIsUnique(name: string) {
		// ...validate...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return name === 'vscode' ? 'Name not unique' : undefined;
	}

	
	async function getAvailableRegions(resourceGroup: QuickPickItem | string, token?: CancellationToken): Promise<QuickPickItem[]> {
		// ...retrieve...
		//await new Promise(resolve => setTimeout(resolve, 1000));
		return ['ap-southeast-1']
			.map(label => ({ label }));
	}

    const state = await collectInputs();

    let cliRegion = '';
    if(region.length>0) cliRegion = region;
    else cliRegion = state.region.label;
    
    startBuild();

    async function startBuild(){
        await vscode.workspace.getConfiguration().update('conf.view.awsAPIGatewayID',state.gateway, true);
        const subnet = vscode.workspace.getConfiguration().get('conf.view.awsAPIGatewayID') as string;
        updateStatusBarAPIGatewayItem();
        window.showInformationMessage('API Gateway ID Updated.');
        return null;
    }
}

// -------------------------------------------------------
// Helper code that wraps the API for the multi-step case.
// -------------------------------------------------------


class InputFlowAction {
    private constructor() { }
    static back = new InputFlowAction();
    static cancel = new InputFlowAction();
    static resume = new InputFlowAction();
}

type InputStep = (input: MultiStepInput) => Thenable<InputStep | void>;

interface QuickPickParameters<T extends QuickPickItem> {
    title: string;
    step: number;
    totalSteps: number;
    items: T[];
    activeItem?: T;
    placeholder: string;
    buttons?: QuickInputButton[];
    shouldResume: () => Thenable<boolean>;
}

interface InputBoxParameters {
    title: string;
    step: number;
    totalSteps: number;
    value: string;
    prompt: string;
    validate: (value: string) => Promise<string | undefined>;
    buttons?: QuickInputButton[];
    shouldResume: () => Thenable<boolean>;
}

class MultiStepInput {

    static async run<T>(start: InputStep) {
        const input = new MultiStepInput();
        return input.stepThrough(start);
    }

    private current?: QuickInput;
    private steps: InputStep[] = [];

    private async stepThrough<T>(start: InputStep) {
        let step: InputStep | void = start;
        while (step) {
            this.steps.push(step);
            if (this.current) {
                this.current.enabled = false;
                this.current.busy = true;
            }
            try {
                step = await step(this);
            } catch (err) {
                if (err === InputFlowAction.back) {
                    this.steps.pop();
                    step = this.steps.pop();
                } else if (err === InputFlowAction.resume) {
                    step = this.steps.pop();
                } else if (err === InputFlowAction.cancel) {
                    step = undefined;
                } else {
                    throw err;
                }
            }
        }
        if (this.current) {
            this.current.dispose();
        }
    }

    async showQuickPick<T extends QuickPickItem, P extends QuickPickParameters<T>>({ title, step, totalSteps, items, activeItem, placeholder, buttons, shouldResume }: P) {
        const disposables: Disposable[] = [];
        try {
            return await new Promise<T | (P extends { buttons: (infer I)[] } ? I : never)>((resolve, reject) => {
                const input = window.createQuickPick<T>();
                input.title = title;
                input.step = step;
                input.totalSteps = totalSteps;
                input.placeholder = placeholder;
                input.items = items;
                if (activeItem) {
                    input.activeItems = [activeItem];
                }
                input.buttons = [
                    ...(this.steps.length > 1 ? [QuickInputButtons.Back] : []),
                    ...(buttons || [])
                ];
                disposables.push(
                    input.onDidTriggerButton(item => {
                        if (item === QuickInputButtons.Back) {
                            reject(InputFlowAction.back);
                        } else {
                            resolve(<any>item);
                        }
                    }),
                    input.onDidChangeSelection(items => resolve(items[0])),
                    input.onDidHide(() => {
                        (async () => {
                            reject(shouldResume && await shouldResume() ? InputFlowAction.resume : InputFlowAction.cancel);
                        })()
                            .catch(reject);
                    })
                );
                if (this.current) {
                    this.current.dispose();
                }
                this.current = input;
                this.current.show();
            });
        } finally {
            disposables.forEach(d => d.dispose());
        }
    }

    async showInputBox<P extends InputBoxParameters>({ title, step, totalSteps, value, prompt, validate, buttons, shouldResume }: P) {
        const disposables: Disposable[] = [];
        try {
            return await new Promise<string | (P extends { buttons: (infer I)[] } ? I : never)>((resolve, reject) => {
                const input = window.createInputBox();
                input.title = title;
                input.step = step;
                input.totalSteps = totalSteps;
                input.value = value || '';
                input.prompt = prompt;
                input.buttons = [
                    ...(this.steps.length > 1 ? [QuickInputButtons.Back] : []),
                    ...(buttons || [])
                ];
                let validating = validate('');
                disposables.push(
                    input.onDidTriggerButton(item => {
                        if (item === QuickInputButtons.Back) {
                            reject(InputFlowAction.back);
                        } else {
                            resolve(<any>item);
                        }
                    }),
                    input.onDidAccept(async () => {
                        const value = input.value;
                        input.enabled = false;
                        input.busy = true;
                        if (!(await validate(value))) {
                            resolve(value);
                        }
                        input.enabled = true;
                        input.busy = false;
                    }),
                    input.onDidChangeValue(async text => {
                        const current = validate(text);
                        validating = current;
                        const validationMessage = await current;
                        if (current === validating) {
                            input.validationMessage = validationMessage;
                        }
                    }),
                    input.onDidHide(() => {
                        (async () => {
                            reject(shouldResume && await shouldResume() ? InputFlowAction.resume : InputFlowAction.cancel);
                        })()
                            .catch(reject);
                    })
                );
                if (this.current) {
                    this.current.dispose();
                }
                this.current = input;
                this.current.show();
            });
        } finally {
            disposables.forEach(d => d.dispose());
        }
    }
}


